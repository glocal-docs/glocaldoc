# Dipendenti

## Pagamento cedolini
- Arriva via email da consulente del lavoro il libro unico del lavoro in duplice copia: una per la ditta e l'altra da consegnare ai collaboratori 
- La copia della ditta (è indicato sul nome del documento) si carica in netxtcloud/amministrazione/cedolini
- Il PDF per i collaboratori va diviso in modo tale da consegnare a ciascuno il proprio 
- Si procede al bonifico
 indicando come causale "cedolino" - mese - anno"

>Tempo stimato 20-30 minuti 
