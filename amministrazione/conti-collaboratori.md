# Conti collaboratori
- In drive aprire la cartella "glocal" poi "amministrazione" 
- Aprire il foglio google del collaboratore di riferimento
- Verificare le attività, ultimo pagamento effettuato e che gli importi siano corretti
- Evidenziare in verde le voci che sono ok, in giallo quelle che necessitano di un confronto con il collaboratore 
- Per reparto commerciale: verificare fattura per fattura che esista su fattureInCloud e aggiornare il referente su eventuali pagamenti non ancora ricevuti
- Dopo eventuale confronto con il collaboratore e aver analizzato eventuali situazioni incerte o che presentano dei problemi, autorizzare il collaboratore a fare la fattura 