# Analisi incassi settimanale
- Si fa una volta a settimana 
- Accedere al conto in banca e verificare tutte le entrate dall'ultima registrata 
- Copiarne una per una e incollarla nella chat su slack con Enrico e Nicolas 
- Accedere al conto Paypal e fare la stessa operazione
- Accedere a Stripe e fare la stessa operazione 
- Una volta incollate nella chat su Slack registrarne una per una su Fatture In Cloud inserendo data e metodo di pagamento
- **Verificare sempre** la task "recupero crediti" e chiudere eventuali task inerenti a fatture ancora da pagare. In questo caso avvisare il referente cliente in modo che sia informato sul pagamento effettuato