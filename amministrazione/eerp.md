# EERP

## Dati

SID: K95IV18

## Generazione link pagamento

* vai su [crm.glocalweb.it](https://crm.glocalweb.it)
* dal menu principale, clicca su Vendite
* da Vendite fai Crea - questo creerà un nuovo ordine di vendita
* nell'ordine di vendita scegli l'azienda a cui vuoi vendere il prodotto
* nella sezione sottostante scegli il prodotto da associare all'ordine \(verifica sempre che l'importo sia corretto\)
* fai Salva e poi Conferma - questo passo rende l'ordine di vendita accettato
* dalla tendina in alto scegli Azione e poi Generate paymentl link
* dopo il codice indicato, inserisci FATT+NumeroFattura es: FATT365, senza usare spazi
* clicca in un punto vuodo del riquadro per aggiornare il link
* clicca su "Copia link"
* invia il link per il pagamento tramite mail cliente \(preferibilmente nella mail accompagnatoria della fattura\)
* se il cliente ha lasciato i dati della sua carta, puoi aprire il link in una finestra in incognito e procedere tu a fare il pgamento
* torna nel menu iniziale e scegli Iscrizioni
* togli il filtro Le mi iscrizioni, e cerca la nuova sottoscrizione creata
* clicca su Modifica
* inserisci come data di inizio la data di pubblicazione \(della pagina personale o della relativa scadenza del prodotto\) e come "data prossima fattura" lo stesso giorno dell'anno successivo

## Fatturazione

### Voci conto attive

- Mantenimento siti web
- Commissioni E-Commerce
- Vendite merci
- Realizzazione Siti Web
- Campagne Pubblicitarie
- Pagine Personali LoveLanghe
- Campagne Pubblicitarie LoveLanghe
- Contributi in conto esercizio (bandi)

### Voci conto costi
- merci c/acquisti
- Hosting & Housing
- Costi per utilizzo Licenze
- Costi pubblicitari clienti
- Costi pubblicitare LoveLanghe
- GW prestazioni di  terzi e professionisti afferenti all'attività
- LL prestazioni di  terzi e professionisti afferenti all'attività
- spese telefonia
- Personale c/retribuzioni -> pagamenti cedolini
- Debiti v/istituzioni previdenziali -> inps
- Debiti per ritenute da versare -> Ritenute fiscali dipendenti

### Dati di fatturazione

| DATO | Campo ODOO |
| :--- | :--- |
| SDI / Codice univoco | Codice destinatario |

### Aliquote

Configurazione -> Imposte

- non imp art 41 331/93  --> n3.2 - vendita di beni con fatture in Europa
- non imp art 8 fuori territorio comunitario --> n3.1 - vendite di beni fuori europa
- Inversione contabile ai sensi dell'art.7 ter D.P.R. 633/72 n2.1
- operazione non soggetta a IVA ai sensi dell'art.7 ter D.P.R. 633/72 -->  n2.1 servizi extra eu
- Operazioni non imponibili a seguito di presentazione dichiarazione d'intento art 8 comma 1 lettera C --> N3.5

### Note di credito
Si fa dalla fattura originale
- aggiungi nota di credito
- motivo
- click su storna
- salva (se la salvi non viene confermata, se la confermi viene confermata E salvata)

Se ho creato un rimborso parziale, quando ho fatto:
- torno alla fattura 
- dagli importi c'è un tasto "aggiungi"
- click e posso vedere che la fattura si aggiorna stornando la nota di credito

RFATT → indica la nota di credito

### Azioni varie
#### Registrazione fatture passive
- apri fattura
- click su "registra pagamento" (lo stato della fattura diventa "in pagamento")

#### Fatture scadute
clienti > resoconto controllo di pagamento

#### Autofatture
fatture fornitore > nuova > e poi applicare imposta RC

#### Fattura proforma
Si parte dal modulo vendite > nuovo > selezionare cliente e servizio > invia fattura proforma

### Cambiare impostazioni solleciti pagamento:
configurazione > livelli di sollecito
si possono modificare quelli esistenti o crearne di nuovi

#### Cambiare la durata della raccolta delle fatture in entrata
Azioni automatiche > FatturaPA: Ricezione fatture dal Sistema d'Interscambio 

#### Aggiungere / modificare tipi di conto delle fatture
I tipi di conto delle fatture sono delle pseudo categorie. Per aggiungerne di nuovi conviene duplicarne e modificarne uno esisntente (es: merci c/vendite)

Contabilità > Configurazione > Piano Conti

## Varie
### Log Email
Impostazioni > funzioni tecniche > email > email

### Eliminazione definitiva dei campi

Impostazioni > funzioni tecniche > struttura database > campi

## Template
### Campi dinamici

`<t t-out="object.partner_id.contact_address_complete or 'address not available'"> Segnaposto</t>`

Quando devo cercare degli elementi collegati, a volte devo cercare l'oggetto collegato nel primo campo, e nel sottocampo cercare  il campo voluto. Es. nelle sottoscrizioni devo prima cercare "Cliente" nel campo principale, e poi "mail" nel sottocampo per trovare il campo mail {{ object.partner_id.email_formatted }}

`{{ (object.user_id.email_formatted or user.email_formatted) }}`


```
object.name
```

## Modifica firme

Se la mail è diversa creare una nuova azione automatica / se no aggiungere il dominio all'azione già creata. Quello dei contatti è Partner

Viste / message_notification_email
Viste / Mail: Pay Now mail notification template

## Creare fattura fornitore da 0
- crea e compila i campi
- scegli il fornitore
- aggiungi data fattura (se no non si può confermare)
- cambia la valuta se necessario
- registro (si può lasciare “fatture fornitore”)
- campi “riferimento fattura/ricevuta, conto bancario, riferimento pagamento” sono facoltativi)
- autocompletamento - è utile se ci sono delle fatture ricorrenti. Se l’importo è identico, è possibile selezionare la fattura passata e odoo compila in automatico

Alternativa: prima di crearne una nuova, si può caricare la fattura del fornitore, così che, una volta caricata, si possano inserire i dati copiandoli dalla fattura originale, che rimane in una schermata a fianco. E può poi essere allegata

## Codice fatturazione elettronica
https://www.odoo.com/documentation/15.0/applications/finance/accounting/fiscal_localizations/localizations/italy.html

