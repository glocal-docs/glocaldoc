# Recuperare uno SDI e/o un'anagrafica

* Accedere al sito dell'Agenzia delle Entrate
* Dal menu a sx selezionare Servizi Per &gt; Fatture e Corrispettivi
* Cliccare su Accedi
* Selezionare "Incaricato"
* Selezionare la partita IVA di Glocal
* Accetta e Prosegui
* Sotto il box "Fatturazione elettronica e Conservazione" selezionare "Fatturazione Elettronica e Conservazione"
* Nel box "Crea nuovo file" selezionare "Fattura Ordinaria"
* Inserire la P.IVA e clicca su "Recupero dati anagrafici"



