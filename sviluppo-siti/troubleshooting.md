# Troubleshooting

## Mailchimp WooCommerce

#### Errore SSL

nella funzione:

`protected function processCurlResponse($curl)`

eliminare

`curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0)`

Oppure, [approfondisci qui](https://stackoverflow.com/questions/48950599/windows-mamp-php-curl-issue).

## Scripts

Vedere una lista di script del footer

```PHP
// Appear on the bottom, after the footer
add_action( 'wp_footer', 'show_footer_scripts', 9999 );
function show_footer_scripts(){
  global $wp_scripts;
  foreach( $wp_scripts->queue as $handle ) :
      debug($handle);
  endforeach;
}
```

## Restart WSL when hangs

First get the PID of svchost.exe running LxssManager, open the cmd as administrator and run:

```shell
tasklist /svc /fi "imagename eq svchost.exe" | findstr LxssManager
```

Grab the returned PID, then run task manager as administrator, in the details tab, search for the svchost.exe containing the PID, right click it and select 'end process tree'.



