# Inspiration

## Tools
- v0 di vercel

## Misc Websites
- https://www.nocciolaamoremio.com/

## Restaurants

Laurent
https://preview.themeforest.net/item/laurent-elegant-restaurant-theme/full_screen_preview/25400434

## Cantine

Chateau
https://chateau.qodeinteractive.com/ 

## Percorsi
https://www.behance.net/gallery/49256431/Hiking

## Scrapbook?
https://it.pinterest.com/pin/90775748733261796/

## Ghost
https://ghost.org/themes/

## Typography Effects

### Typo con bordino e fondo trasparente

```css
-webkit-text-stroke: 2px #000;
text-stroke: 2px #000;
color: transparent;
```

### Thicccc
https://jdsteinbach.com/wordpress/changing-wordpress-query-vars-specific-archive-pages/

```css
text-shadow: -0.5px 0 blue,
-0.5px 0.5px red,
-1px 1px blue,
-1px 1px red,
-1.5px 1px blue,
-1.5px 1.5px red,
-2px 2px blue,
-2px 2px red,
-2.5px 2px blue,
-2.5px 2.5px red,
-3px 3px blue,
-3px 3px red,
-3.5px 3px blue,
-3.5px 3.5px red,
-4px 4px blue,
-4px 4px red,
-4.5px 4px blue,
-4.5px 4.5px red,
-5px 5px blue,
-5px 5px red;
```
![](../assets/thick_typo.png)
## Illustrations
Free Illustrations https://lukaszadam.com/illustrations

Undraw https://undraw.co/

Humaaans https://www.humaaans.com/

Drawkit https://www.drawkit.io/

Open Doodles https://opendoodles.com

Illustrations.co https://illlustrations.co/

## Forms
![](../assets/forms.webp)