# Classi Elementor

Elementor è il page builder che utilizziamo per impaginare i contenuti sui siti creati e gestiti da noi.

La possibilità di inserire delle **classi CSS personalizzate** su ogni elemento lo rende particolarmente flessibile: in questa sezione trovi le **classi CSS standard** che utiliziamo per modificare e gestire l'aspetto di una pagina.

## Classi Colore

* color--dark - sfondo scuro, tipografia chiara
* color--light - sfondo chiaro \(ma diverso dal default del sito\), tipografia scura
* color--dark-alt - sfondo scuro alternativo, tipografia chiara
* color--light-alt- sfondo chiaro alternativo \(ma diverso dal default del sito\), tipografia scura

#### Dove si utilizzano

Le classi colore possono essere usate:

* nelle sezioni
* nelle colonne
* nei widget di testo

## Classi colonne

### Posizionamento

Nelle colonne all'interno di sezioni a larghezza piena, puoi utilizzare le seguenti classi:

`column--force-right`

`column--force-left`

per far si che il contenuto testuale si allinei con il contenuto delle sezioni boxed.

### Arrow

Creano delle frecce sulla colonna adiacente, con lo stesso colore di sfondo sulla colonna in cui viene inserita la classe.

`has-arrow`

`has-arrow--left`

`has-arrow--right`

Vanno usate in congiunzione: `has-arrow has-arrow--left` oppure `has-arrow has-arrow--right`

### Altezza mobile

La classe `column--mobile-height` deve essere utilizzata sulle colonne che hanno solo un'immagine di sfondo e nessun contenuto.

Aggiungendo la classe, queste colonne su mobile acquistano un'altezza minima che permette all'utente di visualizzare l'immagine intera.

 

## Classi Widget

le classi per i widget controllano l'aspetto e il posizionamento dei singoli widget.

### Posizionamento:

##### DX / SX

* widget--left - collassa il widget sulla sinistra della colonna
* widget--right - collassa il widget sulla destra della colonna

##### TOP / BOTTOM

* widget--mt-lg
  * margin top grande
* widget--mt-xl
  * margin top extra grande
* widget--mb-lg
  * margin bottom grande
* widget--mb-xl
  * margin bottom extra grande
* widget--mv-lg
  * margin top & bottom grande
* widget--mv-xl - margin top & bottom extra grande

### Aspetto:

* widget--wide - un widget più largo di quello di default
* widget--wider - un widget più largo di quello wide
* widget--full - un widget che occupa tutta la larghezza della colonna

### Widget di testo

Sui widget di testo è possibile usare anche le classi di colore per determinarne il background.

## Classi Font

E' possibile usare delle classi specifiche per cambiare l'aspetto delle tag `h1`, `h2`, `h3`, `h4`, `h5`e `p`.

* font--h1 - per i font che devono assomigliare ad un h1



