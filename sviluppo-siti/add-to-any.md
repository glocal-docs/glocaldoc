# Share To Any

Questo plugin aggiunge i pulsanti di condivisione sui social network. Qui di seguito trovi la configurazione di base da utilizzare.

La pagina di configurazione è raggiungibile da `Impostazioni > AddToAny`.

### Tab \#1 - Standard

**Stile icona**: 32

**Background**: transparent.

**Foreground**: original

**Pulsanti condivisione**: FB, Twitter, G+

**Pulsante universale**: nessuna \(Show count: no\)

**Sharing header**: vuoto

**Posizione**: nessuna selezionata

**Opzioni menu**: nessuno

**Javascript aggiuntivo**: niente

**Css aggiuntivo**:

```
.a2a_floating_style  {
    margin-left: 4px;
    background-color: #fff !important;
    border-radius: 0 !important;
    box-shadow: 1px 1px 4px rgba(0,0,0,0.4);
    padding: 6px !important;
}

.a2a_floating_style.a2a_vertical_style {
    top: 45% !important;
}

.a2a_vertical_style a {
    padding: 4px 0 !important;
    border-bottom: 1px solid #dbdbdb;
}

.a2a_vertical_style a:last-child {
    border-bottom: 0;
}

.a2a_default_style .a2a_count,
.a2a_default_style .a2a_svg,
.a2a_floating_style .a2a_svg,
.a2a_menu .a2a_svg,
.a2a_vertical_style .a2a_count,
.a2a_vertical_style .a2a_svg {
    border-radius: 0 !important;
}

.a2a_svg svg {
    background: #fff !important;
}


.a2a_svg svg path {
    fill: #bdbdbd !important;
}
```

**Opzione Avanzate**: nessuna

### Tab \#2 - Floating

**Posizione**: left docked.

**Responsiveness**: selezionato - risoluzione 768

**Posizione**: 100

**Offset**: 0

**Dimensioni icona**: 32px

**Sfondo**: transparent

#### Pulsanti Orizzontali

**Posizione**: Nessuno

**Responsiveness**: checked con 768px

**Posizione**: 0

**Offset**: 0

**Dimensione icona**: 16

**Sfondo**: transparent

