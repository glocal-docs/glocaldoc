# WSL

## Troubleshooting

### WSL Hangs

First get the PID of `svchost.exe` running `LxssManager`:
- open the cmd as administrator
- run: `tasklist /svc /fi "imagename eq svchost.exe" | findstr LxssManager`
- Grab the returned PID
- run task manager as administrator
- in the details tab, search for the `svchost.exe` containing the PID
- right click it and select `end process tree`