# Virtual pages

## Creating a virtual page with a simple URL


This is the simplest type of virtual page since you _do not have to work with custom rewrite rules_. You just need to _get the value of the variable `invoice_id` on the URL and display the template based on that value_.

Note that the variable on the URL must not be the same as the [existing WordPress query variables](https://codex.wordpress.org/WordPress_Query_Vars).

To get a template for that URL, we will hook into `init` as follows:

add\_filter( 'init', function( $template ) {
    if ( isset( $\_GET\['invoice\_id'\] ) ) {
        $invoice\_id = $\_GET\['invoice\_id'\];
        include plugin\_dir\_path( \_\_FILE\_\_ ) . 'templates/invoice.php';
        die;
    }
} );

We use the `init` hook instead of `template_include` so that WordPress does not have to query the database to retrieve messages. Therefore, speed will be faster.

The template file is located in the `templates` directory of the plugin. If you put it in another directory or in the add, change the path to that file.

After including the file, we must run `die` so that WordPress does not execute queries and other tasks (which possibly output to the screen).

This method is **very fast and convenient**. However, its downside is that **the URL is not pretty**.

## Creating a virtual page with rewrite endpoint API
--------

The [Rewrite endpoint API](https://make.wordpress.org/plugins/2012/06/07/rewrite-endpoints-api/) is _a part of the rewrite rules API in WordPress_, which helps us add a suffix (endpoint) into a link structure already available.

For example, at _Meta Box_, we create a custom post type `product` for plugins and each plugin have the following URL structure:

https://metabox.io/plugins/meta-box-builder/

With the endpoint, we can create a URL for the changelog as follows:

https://metabox.io/plugins/meta-box-builder/changelog/

Here `/changelog/` is added at the end of an existing URL and creates a custom URL.

This is very handy because it makes the URL structured and easy to understand. Besides, it is quite simple.

To add an endpoint, we use the [`add_rewrite_endpoint()`](https://codex.wordpress.org/Rewrite_API/add_rewrite_endpoint) function as follows:

add\_action( 'init', function() {
    add\_rewrite\_endpoint( 'changelog', EP\_PERMALINK );
} );

And load the template as follows:

add\_action( 'template\_redirect', function() {
    global $wp\_query;
    if ( ! is\_singular( 'product' ) || ! isset( $wp\_query\->query\_vars\['changelog'\] ) ) {
        return;
    }
    include plugin\_dir\_path( \_\_FILE\_\_ ) . 'templates/changelog.php';
    die;
} );

We use the `template_redirect` hook instead of `init` as we did before because we have to check whether the page being loaded is a single product page. Note that when we add an endpoint to a URL, **all template tags of WordPress still work**. This is very useful when checking the condition of the page.

Similar to the first method, we also include a template file from the `templates` folder of the plugin and `die` so that WordPress does not have to perform extra actions.

Using endpoint is quite **convenient for content types that relate to the available content**. However, it is **limited to existing URL structures** and cannot create custom URL structures. To do that, let’s see the third one below.

## Creating a virtual page using custom rewrite rules
---------

Suppose we need to create a page to download the invoice `domain.com/download/123`, where `123` is the ID of the order. We will need to do the following steps:

### Create a custom rewrite rules

```php
add\_filter( 'generate\_rewrite\_rules', function ( $wp\_rewrite ){
    $wp\_rewrite\->rules = array\_merge(
        \['download/(\\d+)/?$' => 'index.php?dl\_id=$matches\[1\]'\],
        $wp\_rewrite\->rules
    );
} );
```

The filter [`generate_rewrite_rules`](https://codex.wordpress.org/Plugin_API/Action_Reference/generate_rewrite_rules) is used to _add a custom rewrite rule to the list of available WordPress rules_ (stored in `$wp_rewrite->rules`). This list is an array, and to add a rule, we simply add an element to that array.

A rule is defined by two components:

*   **Rewrite rule**: is declared as a regular expression `download/(\d+)/?$`
*   **The parameters when the WordPress parse rule**: `index.php?dl_id=$matches[1]`. WordPress uses the function [`preg_match()`](https://php.net/preg_match) so the parameters are parsed similar to this function. The parameter `$matches[1]` is the value in the first bracket of the rewrite rule, ie the `\d+` part. The value of this parameter is passed to the custom variable `dl_id`.

### Adding a custom query var to WordPress

After parsing the rewrite rule as above, the value of the invoice ID is stored in the `dl_id` variable. To make WordPress understand this variable, we need to declare it as follows:

```php
add\_filter( 'query\_vars', function( $query\_vars ){
    $query\_vars\[\] = 'dl\_id';
    return $query\_vars;
} );
```

This code will add `dl_id` to the [list of WordPress query vars](https://codex.wordpress.org/WordPress_Query_Vars).

### Load template

The final step is loading a template for the download page. This is the code:

```php
add_action( 'template\_redirect', function(){
    $dl\_id = intval( get\_query\_var( 'dl\_id' ) );
    if ( $dl\_id ) {
        include plugin\_dir\_path( \_\_FILE\_\_ ) . 'templates/download.php';
        die;
    }
} );
```

We get the invoice ID by the function [`get_query_var()`](https://codex.wordpress.org/Function_Reference/get_query_var). Since the `dl_id` the variable was declared in the previous step, WordPress understands and derives it. If your URL is `download.com/download/123` then the value of `dl_id` is `123`.

Then we load the corresponding template file and `die` so that WordPress does not handle the next actions.

Compared to the previous method, this last approach is **more complex**. However, this is **the only way you can create your own URL**.

You might ask: How do I create a custom URL like `domain.com/my-custom-url`?

Well, it’s not hard to do that, try the code below:

```php
add_filter( 'generate\_rewrite\_rules', function ( $wp\_rewrite ){
    $wp\_rewrite\->rules = array\_merge(
        \['my-custom-url/?$' => 'index.php?custom=1'\],
        $wp\_rewrite\->rules
    );
} );
add_filter( 'query\_vars', function( $query\_vars ){
    $query\_vars\[\] = 'custom';
    return $query\_vars;
} );
add\_action( 'template\_redirect', function(){
    $custom = intval( get\_query\_var( 'custom' ) );
    if ( $custom ) {
        include plugin\_dir\_path( \_\_FILE\_\_ ) . 'templates/custom.php';
        die;
    }
} );
```