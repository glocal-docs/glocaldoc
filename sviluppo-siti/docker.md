# Docker on Windows & WSL

## Problemi docker

### Porte non libere

1. `net stop winnat`
2. `docker start` ...
3. `net start winnat`


## Problemi di connessione in WSL

#### Soluzione 1

In PS as administrator `Remove-NetNat`

#### Soluzione 2

In PS as administrator

`netsh winsock reset`

`netsh int ip reset all`

`netsh winhttp reset proxy`

`ipconfig /flushdns`

#### Soluzione 3

In WSL

`sudo ifconfig eth0 netmask 255.255.240.0`

`sudo ip route add default via 192.168.112.1`

Where 192.168.112.1 and 255.255.240.0 are the IP and netmask you got on the WSL interface \(run ipconfig on windows and look for the WSL entry\).

