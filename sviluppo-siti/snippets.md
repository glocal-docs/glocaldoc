# Code snippets

## WP

### SMTP

```php 
/**
 * Configure SMTP settings in `wp-config.php`.
 *
 * @param \PHPMailer\PHPMailer\PHPMailer $phpmailer
 * @link https://github.com/WordPress/WordPress/blob/master/wp-includes/PHPMailer/PHPMailer.php
 */
$GLOBALS['wp_filter']['phpmailer_init'][10][] = array(
	'accepted_args' => 1,
	'function'      => function ( $phpmailer ) {
		$phpmailer->Host          = 'smtp.example.com';
		$phpmailer->Port          = 10025;
		$phpmailer->SMTPAuth      = true;
		$phpmailer->Username      = 'login_name';
		$phpmailer->Password      = 'passowrd';
		$phpmailer->SMTPSecure    = true;
		$phpmailer->SMTPKeepAlive = true;

		$phpmailer->IsSMTP();
	},
);
```

## WooCommerce

### Limit product purchase

https://stackoverflow.com/questions/55830859/allow-only-to-purchase-a-product-once-per-customer-in-woocommerce ----> ci sono anche altre soluzioni forse quella che mi serve è la seconda

```php
add_filter( 'woocommerce_variation_is_purchasable', 'products_purchasable_once', 10, 2 );
add_filter( 'woocommerce_is_purchasable', 'products_purchasable_once', 10, 2 );
function products_purchasable_once( $purchasable, $product ) {
    // Here set the product IDs in the array that can be purchased only once 
    $targeted_products = array(**ADD YOUR PRODUCT IDS HERE**);

    // Only for logged in users and not for variable products
    if( ! is_user_logged_in() || $product->is_type('variable') )
        return $purchasable; // Exit

    $user = wp_get_current_user(); // The WP_User Object

    if ( in_array( $product->get_id(), $targeted_products ) &&
    wc_customer_bought_product( $user->user_email, $user->ID, $product->get_id() ) ) {
        $purchasable = false;
    }

    return $purchasable;
}
```
### Apply coupon code if product is in the cart
```php
/**
 * @snippet       Apply a Coupon Programmatically if Product @ Cart - WooCommerce 
 * @how-to        businessbloomer.com/woocommerce-customization
 * @author        Rodolfo Melogli, Business Bloomer
 * @compatible    WC 4.1
 * @community     https://businessbloomer.com/club/
 */
  
add_action( 'woocommerce_before_cart', 'bbloomer_apply_matched_coupons' );
  
function bbloomer_apply_matched_coupons() {
  $coupon_code = 'freeweek'; 
  if ( WC()->cart->has_discount( $coupon_code ) ) return;

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {

    // this is your product ID
    $autocoupon = array( 745 );

    if ( in_array( $cart_item['product_id'], $autocoupon ) ) {   
        WC()->cart->apply_coupon( $coupon_code );
        wc_print_notices();
    }
  }
}
```

### Set / Override Product Price Programmatically
```php
/**
 * @snippet       Alter Product Pricing Part 1 - WooCommerce Product
 * @how-to        businessbloomer.com/woocommerce-customization
 * @author        Rodolfo Melogli, Business Bloomer
 * @compatible    WooCommerce 8
 * @community     https://businessbloomer.com/club/
 */
 
add_filter( 'woocommerce_get_price_html', 'bbloomer_alter_price_display', 9999, 2 );
 
function bbloomer_alter_price_display( $price, $product ) {
    
  // ONLY ON FRONTEND
  if ( is_admin() ) return $price;
  
  // ONLY IF PRICE NOT NULL
  if ( '' === $product->get_price() ) return $price;
  
  // IF CUSTOMER LOGGED IN, APPLY 20% DISCOUNT   
  if ( wc_current_user_has_role( 'customer' ) ) {
    if ( $product->is_type( 'simple' ) || $product->is_type( 'variation' ) ) {        
      if ( $product->is_on_sale() ) {
        $price = wc_format_sale_price( wc_get_price_to_display( $product, array( 'price' => $product->get_regular_price() ) ) * 0.80, wc_get_price_to_display( $product ) * 0.80 ) . $product->get_price_suffix();
      } else {
        $price = wc_price( wc_get_price_to_display( $product ) * 0.80 ) . $product->get_price_suffix();
      }        
  } elseif ( $product->is_type( 'variable' ) ) {
      $prices = $product->get_variation_prices( true );
      if ( empty( $prices['price'] ) ) {
        $price = apply_filters( 'woocommerce_variable_empty_price_html', '', $product );
      } else {
        $min_price = current( $prices['price'] );
        $max_price = end( $prices['price'] );
        $min_reg_price = current( $prices['regular_price'] );
        $max_reg_price = end( $prices['regular_price'] );
        if ( $min_price !== $max_price ) {
            $price = wc_format_price_range( $min_price * 0.80, $max_price * 0.80 );
        } elseif ( $product->is_on_sale() && $min_reg_price === $max_reg_price ) {
            $price = wc_format_sale_price( wc_price( $max_reg_price * 0.80 ), wc_price( $min_price * 0.80 ) );
        } else {
            $price = wc_price( $min_price * 0.80 );
        }
        $price = apply_filters( 'woocommerce_variable_price_html', $price . $product->get_price_suffix(), $product );
      }
    }     
  }
  
  return $price;

}
 
/**
 * @snippet       Alter Product Pricing Part 2 - WooCommerce Cart/Checkout
 * @how-to        businessbloomer.com/woocommerce-customization
 * @author        Rodolfo Melogli, Business Bloomer
 * @compatible    WooCommerce 8
 * @community     https://businessbloomer.com/club/
 */
 
add_action( 'woocommerce_before_calculate_totals', 'bbloomer_alter_price_cart', 9999 );
 
function bbloomer_alter_price_cart( $cart ) {
 
  if ( is_admin() && ! defined( 'DOING_AJAX' ) ) return;

  if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 ) return;

  // IF CUSTOMER NOT LOGGED IN, DONT APPLY DISCOUNT
  if ( ! wc_current_user_has_role( 'customer' ) ) return;

  // LOOP THROUGH CART ITEMS & APPLY 20% DISCOUNT
  foreach ( $cart->get_cart() as $cart_item_key => $cart_item ) {
    $product = $cart_item['data'];
    $price = $product->get_price();
    $cart_item['data']->set_price( $price * 0.80 );
  }
 
}
```

### Automatically Apply Coupon Via URL
```php
/**
 * @snippet       Apply WooCommerce Coupon On Link Click
 * @how-to        businessbloomer.com/woocommerce-customization
 * @author        Rodolfo Melogli, Business Bloomer
 * @compatible    WooCommerce 9
 * @community     https://businessbloomer.com/club/
 */
 
add_action( 'wp_loaded', 'bbloomer_add_coupon_to_session' );
 
function bbloomer_add_coupon_to_session() {  
  if ( empty( $_GET['code'] ) ) return;
  if ( ! WC()->session || ( WC()->session && ! WC()->session->has_session() ) ) {
    WC()->session->set_customer_session_cookie( true );
  }
  $coupon_code = esc_attr( $_GET['code'] );
  WC()->session->set( 'coupon_code', $coupon_code );
  if ( WC()->cart && ! WC()->cart->has_discount( $coupon_code ) ) {
    WC()->cart->calculate_totals();
    WC()->cart->add_discount( $coupon_code );
    WC()->session->__unset( 'coupon_code' );
  }
}
 
add_action( 'woocommerce_add_to_cart', 'bbloomer_add_coupon_to_cart' );
 
function bbloomer_add_coupon_to_cart() {
  $coupon_code = WC()->session ? WC()->session->get( 'coupon_code' ) : false;
  if ( ! $coupon_code || empty( $coupon_code ) ) return;
  if ( WC()->cart && ! WC()->cart->has_discount( $coupon_code ) ) {
    WC()->cart->calculate_totals();
    WC()->cart->add_discount( $coupon_code );
    WC()->session->__unset( 'coupon_code' );
  }
}
```