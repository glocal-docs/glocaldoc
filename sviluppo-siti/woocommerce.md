# WooCommerce

## Single product hooks

**Reference:** https://www.businessbloomer.com/woocommerce-visual-hook-guide-single-product-page/

![wc single hooks](../assets/wc-single-hooks.png)

```php
/**
 * @snippet       List of Default Actions @ WooCommerce Single Product
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @updated       WooCommerce 4.0
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
 
// Before content
add_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
add_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
add_action( 'woocommerce_before_single_product', 'woocommerce_output_all_notices', 10 );
  
// Left column
add_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );
add_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20 );
add_action( 'woocommerce_product_thumbnails', 'woocommerce_show_product_thumbnails', 20 );
 
// Right column
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20 );
 
// Right column - add to cart
do_action( 'woocommerce_before_add_to_cart_form' );
do_action( 'woocommerce_before_add_to_cart_button' );
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
 
add_action( 'woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30 );
add_action( 'woocommerce_grouped_add_to_cart', 'woocommerce_grouped_add_to_cart', 30 );
add_action( 'woocommerce_variable_add_to_cart', 'woocommerce_variable_add_to_cart', 30 );
add_action( 'woocommerce_external_add_to_cart', 'woocommerce_external_add_to_cart', 30 );
add_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 );
add_action( 'woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20 );
do_action( 'woocommerce_before_quantity_input_field' );
do_action( 'woocommerce_after_quantity_input_field' );
do_action( 'woocommerce_after_add_to_cart_button' );
do_action( 'woocommerce_after_add_to_cart_form' );
 
// Right column - meta
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );
do_action( 'woocommerce_product_meta_start' );
do_action( 'woocommerce_product_meta_end' );
 
// Right column - sharing
add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50 );
do_action( 'woocommerce_share' );
 
// Tabs, upsells and related products
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
add_action( 'woocommerce_product_additional_information', 'wc_display_product_attributes', 10 );
do_action( 'woocommerce_product_after_tabs' );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
add_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
 
// Reviews
add_action( 'woocommerce_review_before', 'woocommerce_review_display_gravatar', 10 );
add_action( 'woocommerce_review_before_comment_meta', 'woocommerce_review_display_rating', 10 );
add_action( 'woocommerce_review_meta', 'woocommerce_review_display_meta', 10 );
do_action( 'woocommerce_review_before_comment_text', $comment );
add_action( 'woocommerce_review_comment_text', 'woocommerce_review_display_comment_text', 10 );
do_action( 'woocommerce_review_after_comment_text', $comment );
 
// After content
do_action( 'woocommerce_after_single_product' );
do_action( 'woocommerce_after_main_content' );
```

## Orders
### Accesso a $order

```php
// Get Order ID and Key
$order->get_id();
$order->get_order_key();
 
// Get Order Totals
$order->get_formatted_order_total();
$order->get_cart_tax();
$order->get_currency();
$order->get_discount_tax();
$order->get_discount_to_display();
$order->get_discount_total();
$order->get_total_fees();
$order->get_formatted_line_subtotal();
$order->get_shipping_tax();
$order->get_shipping_total();
$order->get_subtotal();
$order->get_subtotal_to_display();
$order->get_tax_location();
$order->get_tax_totals();
$order->get_taxes();
$order->get_total();
$order->get_total_discount();
$order->get_total_tax();
$order->get_total_refunded();
$order->get_total_tax_refunded();
$order->get_total_shipping_refunded();
$order->get_item_count_refunded();
$order->get_total_qty_refunded();
$order->get_qty_refunded_for_item();
$order->get_total_refunded_for_item();
$order->get_tax_refunded_for_item();
$order->get_total_tax_refunded_by_rate_id();
$order->get_remaining_refund_amount();
  
// Get and Loop Over Order Items
foreach ( $order->get_items() as $item_id => $item ) {
   $product_id = $item->get_product_id();
   $variation_id = $item->get_variation_id();
   $product = $item->get_product(); // see link above to get $product info
   $product_name = $item->get_name();
   $quantity = $item->get_quantity();
   $subtotal = $item->get_subtotal();
   $total = $item->get_total();
   $tax = $item->get_subtotal_tax();
   $tax_class = $item->get_tax_class();
   $tax_status = $item->get_tax_status();
   $allmeta = $item->get_meta_data();
   $somemeta = $item->get_meta( '_whatever', true );
   $item_type = $item->get_type(); // e.g. "line_item", "fee"
}
 
// Other Secondary Items Stuff
$order->get_items_key();
$order->get_items_tax_classes();
$order->get_item_count();
$order->get_item_total();
$order->get_downloadable_items();
$order->get_coupon_codes();
  
// Get Order Lines
$order->get_line_subtotal();
$order->get_line_tax();
$order->get_line_total();
  
// Get Order Shipping
$order->get_shipping_method();
$order->get_shipping_methods();
$order->get_shipping_to_display();
  
// Get Order Dates
$order->get_date_created();
$order->get_date_modified();
$order->get_date_completed();
$order->get_date_paid();
  
// Get Order User, Billing & Shipping Addresses
$order->get_customer_id();
$order->get_user_id();
$order->get_user();
$order->get_customer_ip_address();
$order->get_customer_user_agent();
$order->get_created_via();
$order->get_customer_note();
$order->get_address_prop();
$order->get_billing_first_name();
$order->get_billing_last_name();
$order->get_billing_company();
$order->get_billing_address_1();
$order->get_billing_address_2();
$order->get_billing_city();
$order->get_billing_state();
$order->get_billing_postcode();
$order->get_billing_country();
$order->get_billing_email();
$order->get_billing_phone();
$order->get_shipping_first_name();
$order->get_shipping_last_name();
$order->get_shipping_company();
$order->get_shipping_address_1();
$order->get_shipping_address_2();
$order->get_shipping_city();
$order->get_shipping_state();
$order->get_shipping_postcode();
$order->get_shipping_country();
$order->get_address(); // Returns the requested address in raw, non-formatted way.
$order->get_shipping_address_map_url();
$order->get_formatted_billing_full_name();
$order->get_formatted_shipping_full_name();
$order->get_formatted_billing_address();
$order->get_formatted_shipping_address();
  
// Get Order Payment Details
$order->get_payment_method();
$order->get_payment_method_title();
$order->get_transaction_id();
  
// Get Order URLs
$order->get_checkout_payment_url();
$order->get_checkout_order_received_url();
$order->get_cancel_order_url();
$order->get_cancel_order_url_raw();
$order->get_cancel_endpoint();
$order->get_view_order_url();
$order->get_edit_order_url();
  
// Get Order Status
$order->get_status();
 
// Get Thank You Page URL
$order->get_checkout_order_received_url();
```

### Accesso a $order_id
```php
// Get $order object from order ID
$order = wc_get_order( $order_id );
  
// Now you have access to (see above)...
if ( $order ) {
   $order->get_formatted_order_total( );
   // etc.
   // etc.
}
```

## Images
Ref: https://developer.woocommerce.com/docs/image-sizing-for-theme-developers/

### Image sizing for theme developers

Note: this document was created for use when developing classic themes (as opposed to block themes).

To display images in your catalog, WooCommerce registers a few image sizes which define the actual image dimensions to be used. These sizes include:

- `woocommerce_thumbnail` – used in the product ‘grids’ in places such as the shop page.
- `woocommerce_single` – used on single product pages.
- `woocommerce_gallery_thumbnail` – used below the main image on the single product page to switch the gallery.
- `woocommerce_single` shows the full product image, as uploaded, so is always uncropped by default. It defaults to 600px width. `woocommerce_gallery_thumbnail` is always square cropped and defaults to 100×100 pixels. This is used for navigating images in the gallery. `woocommerce_thumbnail` defaults to 300px width, square cropped so the product grids look neat. The aspect ratio for cropping can be customized by the store owner. It is important to note that despite the actual image widths that are set, themes can ultimately change the size images are displayed using CSS, and image widths may be limited by the product grid/column widths.

###Themes can define image sizes
Starting with WooCommerce 3.3.0, themes can declare what sizes should be used when declaring WooCommerce support. If a theme defines the image sizes (widths), the store owner will not be able to change them, but the size defined should be optimal for the theme.

```php
add_theme_support( 'woocommerce', array(
    'thumbnail_image_width' => 200,
    'gallery_thumbnail_image_width' => 100,
    'single_image_width' => 500,
) );
		add_theme_support( 'woocommerce', array(
    'thumbnail_image_width' => 200,
    'gallery_thumbnail_image_width' => 100,
    'single_image_width' => 500,
) );
```
	
When calling WordPress functions which expect an image size e.g. wp_get_attachment_image_src, you should use the image size names – these are:

- `woocommerce_thumbnail`
- `woocommerce_single`
- `woocommerce_gallery_thumbnail`

Store owners will still be able to control aspect ratio and cropping (see below).

### Changing image sizes via hooks

Whilst themes can fix image sizes at certain widths, and store owners can control widths and aspect ratios, if you need more control over thumbnail sizes there are some hooks available to you. The wc_get_image_size function is used by WooCommerce to get the image size dimensions. The return value of this is passed through a filter: woocommerce_get_image_size_{SIZE_NAME_WITHOUT_WOOCOMMERCE_PREFIX} If using this hook you’ll be passed an array of sizes, similar to this:

```php
array(
    'width' => 200,
    'height' => 200,
    'crop' => 1,
)
```
	
So for example, if I wanted to change gallery thumbnails to be 150x150px uncropped images, you could use the following code:

```php
add_filter( 'woocommerce_get_image_size_gallery_thumbnail', function( $size ) {
    return array(
        'width' => 150,
        'height' => 150,
        'crop' => 0,
    );
} );
```

	
We don’t recommend plugins and themes go this route because it removes control from the store owner and their settings won’t be respected, but the option is there for store owners. Note: after making changes to image sizes you may need to regenerate your thumbnails so the new sizes are used for existing images.

### Changing what image sizes are used in WooCommerce via hooks

As well as the above hook, some template functions in WooCommerce run the image size through a filter so you can use something other than the WooCommerce registered image sizes. The following filters exist:

Filter
- `single_product_archive_thumbnail_size` -	Controls the size used in the product grid/catalog. Default	`woocommerce_thumbnail`
- `subcategory_archive_thumbnail_size` - Controls the size used in the product grid/catalog for category images. Default: `woocommerce_thumbnail`
- `woocommerce_gallery_thumbnail_size` -	Controls the size used in the product gallery, below to main image, to switch to a different image. Defaul to	Array representing the dimensions of the `gallery_thumbnail` image size. Usually `array( 100, 100 )`.
- `woocommerce_gallery_image_size` - Controls the size used in the product gallery. Default	`woocommerce_single`
- `woocommerce_gallery_full_size` -	Controls the size used in the product gallery to zoom or view the full size image.	Default: `full`

*Note*: full is a size registered by WordPress and set in Settings > Media. As an example, let’s say I wanted to make the gallery thumbnail size used the thumbnail size registered by WordPress instead of woocommerce_gallery_thumbnail. The following snippet would do the job:

```php
add_filter( 'woocommerce_gallery_thumbnail_size', function( $size ) {
    return 'thumbnail';
} );
```
	
Note: The hooks listed above are used by WooCommerce core. If a theme has custom template files or uses it’s own functions to output images, these filters may not be in use.

## Troubleshooting

### Timeouts / Long loading times on local installs

WooCommerce, through the `wc-admin` embedded plugin, parse all themes' files. If there are many themes with `node_modules` folders, the time to parse the `/themes/` folder will increase dramatically.

To prevent this behaviour go in `onbboarding.php` in the `packages/woocommerce-admin/src/features` plugin folder, and in the function `has_woocommerce_support` disable the loop.

