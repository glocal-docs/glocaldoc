# Creazione tema figlio di GlocalTheme

Per iniziare a lavorare sul sito di un cliente, è necessario creare un tema figlio partendo dal template "GlocalChild".

Di seguito le istruzioni su come fare.

* vai su gitlab.com e loggati con le tue credenziali
* crea una nuova repository dentro `GlocalWeb/Client Themes/` chiamata `nomecliente` \(es: battaglino\), non inizializzarla con un readme
* vai sul repository di glocalchild \([clicca qui](https://gitlab.com/glocalweb/themes/glocalchild)\) 
* clona il repository dentro il tuo ambiente locale in una cartella `nomecliente`
  * `git clone https://gitlab.com/glocalweb/themes/glocalchild nomecliente`
* entra nel nuovo repository
  * `cd nomecliente`
* cambia l'url remoto del repository appena clonato
  * `git remote set-url origin [URL REPO REMOTA]`
* fai un `git push`  e verifica che non ci siano errori
* cambia il nome del tema dal file /SASS/\_wp\_header.scss
* In `\_webpack/` duplica il file `webpack.config.example.js` e rinomina la copia in `webpack.config.js`. In questo file cambiare il valore dell'host nella variabile \`PROXY\_URL
  \` utilizzando l'URL desiderato di sviluppo locale
* lanciare `npm install`
* lanciare l'ambiente di sviluppo con `npm run dev-cross`



