# Shortcodes

Qui trovi una lista degli shortcode inclusi nel tema Frontend. Per ogni shortcode trovi la lista dei parametri, e dei valori di default per ogni parametro \(se esiste un valore di default\).

#### Con default

I parametri che hanno un default vengono indicati così:

`'parametro' => 'default'`

Il default del parametro è il valore che viene utilizzato nel caso il parametro non venga dichiarato.

## Bottone

Utilizzato per creare un bottone: \[button\].

#### Parametri

```
'type'   => 'full',
'size'   => 'medium',
'color'  => 'default',
'text'   => 'your text here',
'url'    => '#',
'align'  => 'center',
```

## Griglia Vini

Se hai bisogno di creare una lista di vini, devi usare lo shortcode `[wine-grid]`.

#### Parametri

```
'columns'     => 3,
'number'      => 6,
'order'       => 'ASC',
'orderby'     => 'title',
'current'     => 'true',
'color'       => '',
'type'        => '',
'appellation' => '',
'image_type'  => '',
'custom_class' => '',
'pagination'  => 'false',
'position'    => 'shortcode'
```





