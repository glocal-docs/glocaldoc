# Quality Score

https://www.wordstream.com/blog/ws/2021/07/19/does-quality-score-still-matter

Now, before you start frantically searching for a Google announcement that they removed this metric, you can breathe out a sigh of relief because Google Ads Quality Score still exists within accounts.

But is it still relevant?
  
Quality Score has always been considered one of the most elusive performance metrics in Google Ads. And with all the changes to more automation from Google, we haven't heard much on this metric (and what you can manually do to control it) for a while. But does that mean it no longer matters?

I ventured out into the PPC community to answer this question (I have my own opinion, of course) and was pretty surprised by my findings. So read on to find out whether Google Quality Score still matters in 2023! But first, a brief refresher.

### Table of contents

*   [What is Quality Score?](#what)
*   [How does Quality Score work?](#how)
*   [Perspective #1: Google Ads Quality Score does not matter](#one)
*   [Perspective #2: Google Ads Quality Score does still matter](#two)
*   [How to improve your Google Ads Quality Score](#improve)
*   [Is Quality Score going away?](#update)
*   [Is Quality Score still relevant?](#conclusion)

What is Google Ads Quality Score?
---------------------------------

Before we get into our predictions of Quality Score, let's first refresh on [what even goes into a good Quality Score](https://www.wordstream.com/blog/ws/2013/10/01/quality-score-case-study). There are three components to Quality Score - which is **only assigned at the keyword level of standard search campaigns**:

*   **Ad relevance:** how closely your ad matches the search query. So, Google will be looking for your keywords in your ad copy. This is usually considered the easiest "first step" to improving your Quality Score because you can control your copy.
*   **Expected click-through rate (ECTR):** Google's determination of how likely your ad will be clicked on based on things like the query's intent matching to your ad and your historical CTR data. I would consider this the toughest part of Quality Score to improve on since it's up to Google's prediction of your likelihood of pulling in a click, so there's not much you can do to control this.
*   **Landing page experience:** how user-friendly and relevant your landing page is to the searcher. Google will be grading you on portions like page speed load, keywords on the page, relevance, and more. Depending on your resources, this could be considered easy to improve as well since you can hop in your own site to make tweaks to the copy.

Google takes those three factors and compiles them into a score of 1 through 10 (1 being the worst and 10 being the best). Since Google runs a tight ship, I usually see any scores 7 and above being absolutely amazing, 4-6 as decent, and anything below a 4 having room for improvement.

![google ads quality score - google ads auction formula graphic](https://www.wordstream.com/wp-content/uploads/2023/05/google-ads-quality-score-as-auction-formula.jpg)

![google ads quality score - google ads auction formula graphic](https://www.wordstream.com/wp-content/uploads/2023/05/google-ads-quality-score-as-auction-formula.jpg)

How does Quality Score work?
----------------------------

[Quality Score](https://www.wordstream.com/quality-score) impacts your performance because it helps Google determine your [ad rank](https://www.wordstream.com/blog/ws/2013/10/24/adwords-ad-rank-algorithm). **Your ad rank is your Max CPC bid x Quality score**. So, if you're [bidding aggressively](https://www.wordstream.com/blog/ws/2018/12/19/google-ads-automated-bidding) but still see that your [top and absolute top impression rate](https://support.google.com/google-ads/answer/7501826?hl=en) or [impression share loss due to rank](https://www.wordstream.com/blog/ws/2023/06/07/impression-share) metrics are off, I'm betting your keyword's Quality Score is the culprit.

![how the google ads auction works](https://www.wordstream.com/wp-content/uploads/2021/12/does-quality-score-matter-2021-how-google-ads-auction-works.png)

![how the google ads auction works](https://www.wordstream.com/wp-content/uploads/2021/12/does-quality-score-matter-2021-how-google-ads-auction-works.png)

Does Quality Score matter? Two perspectives
-------------------------------------------

There are two schools of thought when it comes to Quality Score, and we're going to walk through them both in detail here.

### Perspective #1: Google Ads Quality Score _does not_ matter

Okay, I'll admit, as a [PPC Consultant](https://localiq.com/blog/marketing/5-questions-to-ask-your-marketing-partner/) I like to play devil's advocate as I'm personally anti-Quality Score. There are a few reasons why:

#### 1\. It's not actionable

It's difficult to control and improve on because it's based on Google's algorithmic judgment. And Google doesn't give us much clarification on what specifically in our ads or [landing page](https://www.wordstream.com/blog/ws/2012/06/04/quality-score-landing-pages-faq) is causing a low score.

#### 2\. It's only applicable to search

Quality Score is **only applicable to standard search campaigns**. Why should search keywords be the only account component that gets held to this KPI? And with all of Google's development over the years, search campaigns are only small fish in a big sea of other [media-heavy campaign](/blog/ws/2021/06/29/google-ads-creative-studio) types like [Performance Max](https://www.wordstream.com/blog/ws/2023/01/23/optimize-performance-max-campaigns), [display](https://www.wordstream.com/blog/ws/2014/11/18/google-display-network-tips) or [video](https://localiq.com/blog/video-marketing-strategy/).

To me, this indicates that there are information gaps in what makes [Google's algorithm](https://www.wordstream.com/blog/ws/2020/09/09/google-ranking-factors) tick when it comes to judging your other campaign types.

#### 3\. It isn't always correlated with performance

I've seen folks get bent out of shape over a low Quality Score when their account is otherwise in great shape and hitting their goals. If you're [pulling in the conversions](https://www.wordstream.com/blog/ws/2019/01/22/conversion-rate-optimizations) that you want at the cost you want, who cares if you have a low Quality Score? The last thing you want is to lose that [stellar performance](https://www.wordstream.com/blog/ws/2014/04/24/ppc-performance) by changing something in order to appease your Quality Score column.

Here are a few examples from real accounts that back my points:

**Missing data**

You'll see in the screenshot below that these keywords all have similar performance metrics. However, Quality Score data is often missing on some keywords, so to identify the differences between a six or a seven you're left splitting hairs.

![example of missing quality score data in google ads reporting](https://www.wordstream.com/wp-content/uploads/2021/12/does-quality-score-matter-2021-quality-score-missing-data-example.png)

![example of missing quality score data in google ads reporting](https://www.wordstream.com/wp-content/uploads/2021/12/does-quality-score-matter-2021-quality-score-missing-data-example.png)

**Inconsistent scores**

In these next three keywords, they're all very similar to each other and go to the same landing page, yet only one gets the ["low quality score" status flag](https://www.wordstream.com/blog/ws/2021/06/23/google-ads-disapprovals), while the others all have widely different scores.

![inconsistent quality score account data in google ads](https://www.wordstream.com/wp-content/uploads/2021/12/does-quality-score-matter-2021-quality-score-inconsistent-account-data.png)

![inconsistent quality score account data in google ads](https://www.wordstream.com/wp-content/uploads/2021/12/does-quality-score-matter-2021-quality-score-inconsistent-account-data.png)

**Score-performance mismatch**

This is one of the [highest converting keywords](https://www.wordstream.com/blog/ws/2017/10/03/most-valuable-keywords-high-ctr) in this account, yet the score is only average.

![google ads quality score average but performance high](https://www.wordstream.com/wp-content/uploads/2021/12/does-quality-score-matter-2021-quality-score-and-account-performance.png)

![google ads quality score average but performance high](https://www.wordstream.com/wp-content/uploads/2021/12/does-quality-score-matter-2021-quality-score-and-account-performance.png)

#### 4\. I'm not the only anti-Quality Scorer

Aaron Babaa, Google Ads Manager at Above All PPC, also doesn't want to waste another minute staring at an arguably arbitrary metric. In his article [Quality Score Is Dead!](https://www.linkedin.com/pulse/quality-score-dead-aaron-babaa/) he makes two points:

**Bot-driven scores are imperfect**

Aaron writes, "Relevancy and landing page experience are bot-driven. That is, we are completely relying on Google's bots to determine if our ads are relevant and if we deliver a satisfactory landing page experience. This system is imperfect. We know this because we have looked at plenty of ads that have low quality scores, but performed and vice versa."

**More score-performance mismatches**

Like me, Aaron has also seen stellar campaigns with low Quality Scores:

"Optimizing for Quality Score often proves to be a wild goose chase. We've seen accounts with crazy-high CTRs, incredibly relevant ads, and beautiful, keyword-optimized landing pages... but terrible Quality Scores. We've even seen accounts where [branded keywords](/blog/ws/2010/09/07/monitoring-search-results-for-brand-keywords) (a keyword that is the name of the company and nothing more), show a Quality Score of 3.

The system is clearly flawed."  

**_[Want to get the full picture on how to improve your Google Ads? Try our FREE Google Ads Performance Grader and get hours of audit insights in seconds!](https://www.wordstream.com/google-adwords?cid=Web_Any_InContentTextLink_PPC_AWGrader_AWGrader)_**

  
And this may be small but one final point: Quality Score isn't even a default column in Google Ads.

![google ads quality score custom column](https://www.wordstream.com/wp-content/uploads/2021/12/does-quality-score-matter-2021-quality-score-custom-columns.png)

![google ads quality score custom column](https://www.wordstream.com/wp-content/uploads/2021/12/does-quality-score-matter-2021-quality-score-custom-columns.png)

### Perspective #2: Google Ads Quality Score _does_ still matter

If there's one thing I've learned about PPC as a consultant, it's that you can't take it too personally. When I asked [Mark Irvine](https://www.wordstream.com/mark-irvine-42) (PPC expert and Director of Paid Media at [SearchLab](https://searchlabdigital.com/)) about his take on the matter, he had strong pro-Quality Score feelings. In fact, he started out by saying:

"Is Quality Score dead? No, not at all."

Here are his reasons for thinking that Quality Score still matters:

#### 1\. Its impact remains unchanged

Mark starts off by pointing out that Quality Score is the same as it's always been.

"It's still an equal factor in ad rank and ad rank still matters if you want to [show up at the top of the SERP](https://www.wordstream.com/blog/ws/2020/08/19/get-on-first-page-google) (or at all). If you've got a poor Quality Score compared to your competition, that'll hurt you as it did before. If you've got a great Quality Score, you'll be better off! The math of that all still holds as it did in 2013 when it was WordStream's anthem."

![google ads ad rank formula](https://www.wordstream.com/wp-content/uploads/2021/12/does-quality-score-matter-2021-quality-score-and-ad-rank.png)

![google ads ad rank formula](https://www.wordstream.com/wp-content/uploads/2021/12/does-quality-score-matter-2021-quality-score-and-ad-rank.png)

#### 2\. Keyword intent has been redefined

Mark goes on to say that while Quality Score hasn't changed since 2013, everything else has:

"It used to be that (in 90 characters of ad text) it was really easy to write a **_bad_**, **_irrelevant, low-quality_** ad. Quality Score was a very simple metric to evaluate how "good" that ad was, and a really useful guidepost to make sure that advertisers were thinking of how their ads matched the searcher's intent.

But since 2013, Google has tripled the size of its ads and added in [customizable ad extensions](https://www.wordstream.com/blog/ws/2013/06/03/adwords-ad-extensions-cheat-sheet), new formats, and [responsive ads](https://www.wordstream.com/blog/ws/2021/03/11/responsive-search-ads-new-default-google-ads) to help testing at scale.

No longer is the question "Does this ad match the search intent across _one keyword signal?"_ but rather "Does this ad match _this particular searcher's intent_ across _thousands of signals_, including keyword, device, operating system, time of day, demographic, \[and more\]?"

#### 3\. The bar for "quality" has been raised

Building on his previous point, Mark says, "There used to be lots of advertisers writing bad ads, so it wasn't difficult to write an ad with a good or great Quality Score to beat them all. Now, Google's made it a lot easier for advertisers to write good (or at least not bad) ads.

That's great for searchers and novice advertisers, but it raises the bar for everyone on [writing a truly great ad](https://www.wordstream.com/blog/ws/2015/04/21/adwords-ads) with a significantly higher Quality Score. [Quality Score still matters](https://www.searchenginejournal.com/ppc-guide/quality-score/#close), it just isn't the silver bullet it used to be."

How to improve Quality Score
----------------------------

One thing we can agree on about Quality Score is the ways to improve it

Mark's last point is in line with one of mine above regarding Google's lack of specificity as to what exactly you need to do to improve your Quality Score.

"Quality Score is still important and the tactics you take to improve it will continue to yield incremental results. But advertisers today are more interested in [PPC strategy](https://localiq.com/blog/marketing-strategies-that-complement-ppc/) than they are in tactics, and that's an exciting thing to see. Improving your Quality Score might still improve your PPC results by 10%, but improving your PPC strategy might 10x them."

> _**Improving your Quality Score might still improve your PPC results by 10%, but improving your PPC strategy might 10x them.**_

WordStream by LocaliQ Associate Director of Managed Services Holly Niemiec sums up this concept nicely:

"I've always viewed Quality Score as more of a ‘check engine light' for campaigns. Meaning yes, it can impact a campaign's performance but isn't the end all be all. Keywords with low Quality Scores can still perform well, and sometimes even better than higher Quality Score keywords. And competitor keywords will always have a [low Quality Score but can still convert well](https://www.wordstream.com/blog/ws/2017/09/19/whats-a-good-quality-score).

I think that making sure your ad copy and keywords are relevant and that you have a landing page that loads quickly is important, and if you have all of those things then that is more important than over-fixating on raising your Quality Score from a 5 to a 7."

Is Google Quality Score going away?
-----------------------------------

Just to reiterate: no! As I mentioned in the intro and as Mark reinforces in his argument, the existence and math for Quality Score is still very much there and the same as it's ever been.

I wrote this post not in response to any information Google has released (in fact, Google's lack of any news on this metric over the past 10 years makes me wonder…but I digress)—but because clearly, I'm not the only one who has been wondering where Quality Score stands with Google and where it will stand in the future.

So is Google Ads Quality Score still relevant?
----------------------------------------------

So, you got two sides to the story today, and now it's totally your call whether you choose to focus on Quality Score or not. If you're struggling to get your ads to show up on the SERP, [improving your Quality Score](https://www.wordstream.com/adwords-quality-score) can help. On the opposite end, if you're comfortable with your account then you may want to let your Quality Score exist as is.

That said, we recently saw at [Google Marketing Live 2023](https://www.wordstream.com/blog/ws/2023/05/23/google-marketing-live) that the search ad space is rapidly changing. Now, there are tons of ways advertisers can promote their business on Google beyond standard search campaigns. How you choose to lean into Google Quality Score depends on your account's unique needs, but you should also try embracing additional advertising strategies, like AI-powered ads, to optimize for success beyond just Quality Score!