# Tipi di Offerta
**Ref**: https://www.wordstream.com/blog/ws/2018/12/19/google-ads-automated-bidding

## Offerte Automatiche

**Ref**: [https://support.google.com/adwords/answer/2979071](https://support.google.com/adwords/answer/2979071)

Con le tipologie di bid \(offerte\) automatiche, le offerte vengono gestite da Adwords in modo da raggiungere un obiettivo specifico.

Per esempio, se non vuoi impostare un CPC massimo per ogni keyword, puoi decidere di utilizzare una strategia Target CPA, impostando solamente il tuo costo di conversione massimo \(il Target CPA\), e Adwords ottimizzerà le offerte per mantenere questo obiettivo.

| **OBIETTIVO** | **STRATEGIA DI OFFERTA** | **LIVELLO** |
| :--- | :--- | ---: |
| Aumenta le visite al sito | **Maximize Click** imposta le tue offerte automaticamente, in modo da farti ottenere il massimo di click possibili con il tuo budget. | Campagna, Gruppo di Annunci, Parola chiave |
| Aumentare la visibilità sulla prima pagina o nelle prime posizioni della pagina di ricerca | **Target Search Page Location** imposta automaticamente le offerte in modo da aumentare la possibilità che i gli annunci compaiano all'inizio della prima pagina. | Campagna, Gruppo di Annunci, Parole chiave \(solo search network\) |
| Aumenta la visibilità rispetto ad altri domini | **Target Outranking Share**: scegli un tuo competitor che vuoi "battere" in termini di posizionamento degli annunci. Adwords imposterà le offerte automaticamente in modo da togliergli visibilità. | Campagna, Gruppo di Annunci, Parole chiave |
| Ottieni più conversioni entro un CPA \(Cost Per Action\) massimo. | **Target CPA\*** imposta automaticamente le tue offerte in modo da farti ottenere il numero massimo di conversioni entro il CPA definito | Campagna, Gruppo di Annunci |
| Aumenta le conversioni, ma mantieni il controllo delle offerte per le parole chiave. | **Enhanced CPC \(ECPC\)\*** corregge automaticamente le bid manuali in modo da farti ottenere più conversioni, cercando di mantenere lo stesso livello di Cost per Conversioni. | Campagna, Gruppo di Annunci |
| Raggiungi un livello di ritorno di investimento \(ROAS - return on an ad spend\) | **Target ROAS\*** imposta le offerte in modo da farti ottenere le conversioni più redditizie secondo un ROAS preimpostato manualmente. Utile quando le conversioni hanno valori diversi una dall'altra. | Campagna, Gruppo di Annunci, Parole chiave |
| Ottieni più conversioni con il tuo budget | **Maximize Conversions** imposta automaticamente le offerte in modo da farti ottenere il massimo numero di conversioni per il budget preimpostato. | Campagna, Gruppo di Annunci, Parole chiave |

**\***Queste offerte sono strategie di "Smart Bidding" che utilizzano algoritmi di Machine Learning avanzati per aiutarti a ritagliare la **giusta bid su ogni singola auction**. Puoi leggere di più sulle [smart bid per la rete di ricerca **qui**](http://services.google.com/fh/files/helpcenter/aw_search_automated_bidding_guide.pdf "Guida di Google per lo Smart Bidding su Rete di Ricerca") \([**qui** la guida completa per la rete display](https://services.google.com/fh/files/misc/display_automated_bidding_guide.pdf)\).



## Maximize Clicks

This strategy is very similar to Maximize Conversions but instead focuses on clicks. With Maximize Clicks, Google will work to get as many clicks as possible while spending your daily budget.

This strategy can be great if you're trying to drive more volume to your site for branding and list building, or if you have very strong conversion performance and want to find more volume.

Advertisers can set a max CPC limit to help keep CPCs down while Google spends the daily budget.

### Cautions with Maximize Clicks

Always set a max CPC and keep an eye on your average max CPC as well. Google will work to get as many clicks as possible for your campaign, but as with Maximize Conversions, it will also work hard to spend your entire daily budget each day, even if clicks are far more expensive than normal.

Regularly check in on the CPC performance this bid strategy is generating (and any other goal metrics you have in place) to ensure it's still meeting your goals. If not, then adjust your settings or potentially look to a new bid strategy.