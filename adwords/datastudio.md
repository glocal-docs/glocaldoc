# Report clienti

Per creare i rapporti da inviare ai clienti, utilizziamo [DataStudio](https://datastudio.google.com/), un **applicativo di Google** che permette di raccogliere dati da diverse sorgenti e metterli insieme in un unico grafico/tabella.

**IMPORTANTE**: I rapporti vanno creati nell'account Datastudio/Drive di adwords@langhe.net, e poi condivisi con il tuo account personale.

## Anatomia di un report

Un report è formato da:

* Un file di DataStudio - che porta il nome del cliente
* Un foglio di calcolo di Google Drive - denominato "Analisi \[NOME CLIENTE\]"
* I dati ripresi dall'account di Google Anlytics del cliente
* I dati ripresi dall'account di Google Adwords collegato alla campagna

## Creazione di un nuovo rapporto

### Foglio di calcolo

Il foglio di calcolo su Google Drive contiene le informazioni inviate dal cliente, e viene compilato ogni quadrimestre.

Ogni riga contiene i dati per il quadrimestre, il cui inizio è riportato nella colonna "Data".

| COLONNA | DATO |
| :--- | :--- |
| Colonna A - Data | Il formato della data è AAAA-MM-GG. Indica l'inizio del quadrimestre a cui sono riferiti i dati delle altre colonne. |
| Colonna B - Modulo | Il numero di visite effettive arrivate dal modulo. |
| Colonna C - Telefono | Il numero di visite effettive arrivate via telefono |
| Colonna D - Langhe.net | Il numero di visite effettive arrivate tramite modulo della pagina personale |
| Colonna E - Passaggio | Visite non prenotate, ma di persone di "passaggio" |
| Colonna F - Altro | Visite arrivate da altri canali \(Tour operator, portali, ecc\) |
| Colonna G - Visite Totali | Somma della colonna B, C, D, E, F. |
| Colonna H - Presenze | Le presenze totali delle visite |
| Colonna I - Visite perse | Le visite prenotate ma che non sono state effettuate |
| Colonna J - Fatturato Totale | Il fatturato generato dalle visite |

Per creare un nuovo file, puoi semplicemente duplicarne uno esistente, ricordandoti di rinominarlo.

#### Dati da richiedere al cliente

Tramite l'invio del file chiamato "modulo-consuntivo-visite" in Langhe-clienti su DB, i clienti ci inviano i dati relativi a:

* Nr. visite totali 
* Nr. presenze totali
* Nr. prenotazioni perse
* Fatturato generato

### Importazione dati pagine personali

Esporta i moduli da Langhe.net \(SB -  Visita in Azienda ITA / ENG\) con i seguenti dati:

* data registrazione
* email azienda
* numero di persone

Una volta scaricati i file \(ITA e ENG\), crea un unico file ed elimina gli apici, così da poter convertire la data nel seguente formato: **gg/mm/aaaa**

Per eliminare gli apici \(che non sono parte del contenuto della cella, ma indicano che la cella è formattata come testo\):

* seleziona tutte le celle e clicca su modifica &gt; trova e sostituisci
* in trova inserisci .\* e in sostituisci &
* accertati che nel campo in ‘altre opzioni’ sia spuntato ‘solo nella selezione’ e ‘espressioni regolari’ 
* clicca su ‘sostituisci tutto’

**N.B.** apri il file csv spuntando: ‘virgola’ e unicode \(UTF 8\)

I dati scaricati devono essere importati su Drive nel file "Compilazioni Modulo Langhe.net".

Data Studio è impostato in modo tale da pescare i dati che servono direttamente da questo file.

### Creazione report

1. Apri un rapporto-modello e clicca su duplica in alto a destra \(c'è l'icona\).
2. Nella finestra che si apre devi collegarele nuove origini dei dati:
   1. Account Adwords → da modificare con l'account e la campagna del cliente
   2. Account di Analytics → da modificare con l'account del cliente
   3. File di Google Drive \(solo dati clienti\) → da collegare al file di Drive del cliente
   4. Se non trova il file di drive, nella prima colonna cliccare su "di mia proprietà". Così dovrebbe uscire.
   5. Compilazioni Modulo Langhe.net - Riepilogo
3. Clicca su crea nuovo rapporto
4. Rinomina il file di Data Studio
5. Condividi il rapporto con i nostri account personali

### Impostazione pagina report

Una volta duplicato il rapporto, bisogna reimpostare alcune opzioni e metriche. Di seugito trovi le istruzioni.

**IMPORTANTE**: per far si che i selettori di data, campagna, etc. funzionino, le tabelle e i selettori devono essere raggruppati insieme.

**NB**: per alcune metriche di questa pagina è stato impostato un filtro sulla campagna Adwords tramite l'ID della campagna. Quando si prende l'ID da Adwords puoi segnarlo anche nel file inserzionista del cliente, così da non doverlo cercare tutte le volte.

**NB**: per evitare che in mancanza di dati esca la scritta "nessun dato" seleziona la casella interessata, nel pannello a destra seleziona la tab "Stile" e alla voce "Dati mancanti" scegli "Mostra 0".

**NB**: i testi in corsivo indicano le metriche personalizzate

#### Voce "Rendimento Campagne Adwords"

| VOCE TABELLA | DATO | SORGENTE |
| :--- | :--- | :--- |
| Impressions | numero delle visualizzazioni degli annunci | Adwords - impressioni |
| Click | traffico alla landing page | Adwords - Clic |
| Click through | % dei click sulle impression | Adwords - nostra metrica omonima \(clic / impressioni \* 100\) |
| Conversioni Adwords | moduli + chiamate annunci | Adwords - Conversioni |
| Conversioni chiamate |  | Analytics - Chiamata \(completamento obiettivo nr\) + Filtro ID Campagna Adwords \(modificare il filtro e inserire l'ID della campagna corretta\) |
| _% di conversione_ | chiamate e prenotazioni / click | _da calcolare a mano dopo aver visualizzato i dati con l'intervallo di tempo corretto_ **\(\*1\)** |

**\(\*1\)** Formula: Conversioni Adwords \(moduli + chiamate annunci\) / Click - traffico alla landing page \* 100

#### Voce "Rendimento Sito"

| VOCE TABELLA | DATO | SORGENTE |
| :--- | :--- | :--- |
| Traffico alla pagina | il traffico alla pagina della visita | Analytics - Visualizzazioni di pagina + Filtro pagina visite in cantina \(nel filtro bisogna mettere anche gli url delle vecchie pagine presi da Analytics - non mettere le LP\) |
| Conversioni sito \(n. prenotazioni IT-EN\) | Include compilazioni moduli e chiamate | Analytics - nostra metrica _Conversioni totali_ + Filtro Esclude dati Adwords \(ID campagna\) \(modificare il filtro e inserire l'ID della campagna corretta\) **\(\*1\)** |
| _% di conversione \(rapporto n. pren./visite\)_ |  | da calcolare a mano dopo aver visualizzato i dati con l'intervallo di tempo corretto **\(\*2\)** |
| Di cui provenienti da Langhe.net | sono le conversioni avvenute sul sito del cliente che hanno come sorgente di traffico langhe.net | impostare una tabella che abbia come dimensione la "sorgente" del traffico e come metriche le "sessioni" e le "_Conversioni totali_" \(nostra metrica già creata in precedenza\) |
| Conversioni Pagina Personale su Langhe.net **\(\*3\)** | Il numero di prenotazioni ricevute sulla pagina personale di Langhe.net | il dato è collegato a un file su drive uguale per tutti i clienti chiamato "Compilazioni Modulo Langhe.net". Il file va aggiornato periodicamente tramite l'esportazione delle registrazione dei moduli ITA & ENG di Langhe.net. |

**\(\*1\)** Bisogna creare la metrica \_Conversioni total\_i con le metriche dei completamenti di obiettivo per le visite in cantina di tutte le lingue e le chiamate. I nomi delle metriche corrispondono ai nomi degli obiettivi impostati su Analytics.

**\(\*2\)** Formula: Conversioni sito \(n. prenotazioni IT-EN\) / Traffico alla pagina \* 100

**\(\*3\)** Opzionale

#### Voce "Rendimento Complessivo Progetto

"

| VOCE TABELLA | DATO | SORGENTE |
| :--- | :--- | :--- |
| Traffico totale alle pagine | Il traffico totale delle pagine legate alla visita | Analytics - visualizzazioni di pagina + Filtro Visite e LP \(modificare il filtro e inserire gli url di tutte le pagine di visita + tutte le LP, vecchie e nuove, qualsiasi lingua\) |
| Conversioni totali | Tutte le conversioni \(incluso i click sul numero di telefono\) | Analytics - nostra metrica _Conversioni totali_ senza filtri |
| _% conversione totale_ | rapporto n. prenotazioni/visite | da calcolare a mano dopo aver visualizzato i dati con l'intervallo di tempo corretto **\(\*1\)** |

**\(\*1\)** Formula: Conversioni totali \(include il click sul numero di telefono\) / Traffico totale alle pagine \* 100

#### Voce "Canali"

Queste voci vengono collegate direttamente al file di Drive, tranne "_Presenze Medie_" e "_Fatturato Medio a Visita_" che sono metriche calcolate rispettivamente:

* Presenze / Visite
* Fatturato / Visite

## Modifiche al report

### Eliminazione di dati

I dati del report sono raggruppati insieme ai selettori delle date.

Per eliminarli, devi cliccarci sopra due volte \(ma non con "doppio click"\):

1. La prima volta serve per selezionare il gruppo
2. La seconda per selezionare l'elemento del dato in questione



