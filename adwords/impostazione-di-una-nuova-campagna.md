# Impostazione di una nuova campagna Adwords

Inizia con un brainstorming per individuare delle possibili parole chiave. Una volta buttato giù un primo elenco, aaccedi ad Adwords e verifica le ricerche medie mensili dei termini individuati, segnando anche il costo medio.

## Pianificazione delle parole chiave

1. Loggati su Adwords ed entra nell'account di un cliente qualsiasi
2. In alto a destra sceglie la chiave inglese e, sotto "pianificazione", nella prima colonna seleziona lo "strumento di pianificazione delle parole chiave"
3. Sulla nuova pagina scegli "visualizza i dati e le tendenze del volume di ricerca", sotto la prima opzione "trova nuove parole chiave"
4. Inserisci un termine di ricerca e la località Italia nelle opzioni. Salva e cicca su "mostra volume di ricerca"
5. Nella schermata successiva avremo i dati di riferimento dell'ultimo anno. Se non vengono caricati, clicca sulla tab "idee per i gruppi di annunci" e ritornare sulla precedente fino a quando non carica i risultati.
6. A parte, su un file Excel, segna i dati relativi a ciascuna parola chiave. Vedi un esempio qui: [Tom Roger](https://docs.google.com/spreadsheets/d/1YO8Pf77we4XrH4x1HVq14ySzVgvaLsH8pOYUJYPiRJc/edit#gid=0)

## Cercare suggerimenti per nuove parole chiave

1. Ripeti i passaggi 1 e 2 del paragrafo precedente.
2. Sulla nuova pagina scegli "cerca nuove parole chiave utilizzando una frase, un sito web o una categoria", sotto la prima opzione "trova nuove parole chiave"
3. Inserisci i dati relativi al prodotto/servizio da pubblicizzare e clicca su "trova idee". 
4. Nella schermata successiva avremo i dati di riferimento dell'ultimo anno. Se non vengono caricati, clicca sulla tab "idee per i gruppi di annunci" e ritornare sulla precedente fino a quando non carica i risultati.
5. Riporta i dati delle parole chiave più interessanti e corenti sul file Excel creato in precedenza. 

## Scegliere le parole chiave e formare i gruppi

1. Ordina le parole chiave con criterio decrescente, secondo le medie delle ricerche mensili
2. Somma tutte le medie delle ricerche e passa all'analisi dei termini di ricerca. 
3. Generalmente, quelli troppo generici vanno esclusi. 
   1. Ad esempio: escludiamo "ciondolo" per la Campagna di Tom Roger, ma teniamo "ciondolo diamante".
4. Suddividi le parole chiave per gruppi omogenei. Ad esempio, secondo il tipo di prodotto o un target preciso. La Campagna di Tom Roger è stata suddivisa in: 
   1. ciondolo, 
   2. collana, 
   3. collier, 
   4. gioiello, 
   5. girocollo,
   6. punto luce, 
   7. regalo e 
   8. uomo.
5. Per ciascun gruppo segna le parole chiave da utilizzare, secondo il tipo di corrispondenza \(BMM, EM, PM\). Per saperne di più [leggi la guida di Adwords](https://support.google.com/adwords/answer/2497836?hl=it). 
6. Per ciascun gruppo calcola il totale delle ricerche per le parole chiave scelte e la media dell'offerta consigliata.
7. Confronta il totale delle ricerche dei gruppi con quello calcolato all'inizio e valuta se il dato è soddisfacente ai fini della campagna da impostare. A seconda del risultato, potrebbe essere necessario sfoltire ulteriorimente i gruppi/parole chiave o, al contrario, aggiungerne di nuovi. 



