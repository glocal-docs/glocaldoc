

## Scrittura degli Annunci

Linee guida in caso di scrittura di annunci per una nuova campagna, oppure di revisione degli annunci creati negli anni passati.

### Referenze per annunci RSA
- https://www.wordstream.com/blog/ws/2018/07/10/responsive-search-ads
- https://smarter-ecommerce.com/blog/en/google-ads/responsive-search-ads-vs-expanded-text-ads/
- https://www.thinkwithgoogle.com/intl/en-apac/marketing-strategies/search/responsive-search-ads-tips/
- https://www.searchenginejournal.com/how-to-optimize-google-responsive-search-ads-rsas/455057/ -- no remarkable
- https://www.wordstream.com/dynamic-keyword-insertion


### Visualizzazione degli Annunci

Gli annunci sono composti da: 

* 2 titoli di 30 caratteri ciascuno, separati da un - Quindi non bisogna scrivere una frase "spezzata" sui due campi
* 1 descrizione di 80 caratteri
* 2 campi URL per personalizzare l'indirizzo, di 15 caratteri ciascuno: usare le lettere maiuscole

Su mobile i titoli possono venire tagliati perchè i caratteri utilizzati non sono mono-space \(quindi 3 ooo occupano più spazio di 3 iii\). Avanza 3/4 caratteri in meno sul totale dei 2 titoli \(60 caratteri\) per evitare che vengano mostrati i ... di sospensione. 

###  Scrittura degli Annunci

* Utilizza il file su DB &gt; Langhe - Campagne Adwords &gt; Modelli vuoti &gt; modello campagna
* Prima di scrivere l'annuncio pensa al concetto che vuoi esprimere. Ad esempio per Tom Roger:
  * esclusività del prodotto --&gt; scrivi un annuncio dove si parla dell'Edizione Limitata del ciondolo in 300 esemplari
  * personalizzazione --&gt; scrivi un annuncio dove si parla dell'incisione del tuo nome sul ciondolo
* Crea annunci che siano abbastanza diversi tra di loro. 
* Non pensare troppo a lungo, butta giù quello che ti viene in mente. Potrai cancellare ciò che non ti convince in un secondo momento.  
* Non è il caso di includere il nome dell'azienda in titolo o descrizione, perchè viene già visualizzato in automatico dall'annuncio.
* Evita ripetizioni nei titoli
* Se possibile, avanza 3/4 caratteri sulla somma totale dei titoli.
* Scrivi almeno 5-6 annunci per gruppo.
* Non seguire le regole usate per la scrittura degli "slug veri" nel campo "percorso personalizzato". Pensa alla facilità di lettura di chi lo legge.
* Utilizza le maiuscole nei titoli e nel percorso personalizzato per mettere in evidenza determinate parole. 
* Per maggiori informazioni vedi la documentazione di Adwords:[https://support.google.com/adwords/answer/1704389?hl=it](https://support.google.com/adwords/answer/1704389?hl=it)



