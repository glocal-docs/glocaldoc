# Campagne Google Shopping

\[DESC\]

## Passi per setup

1. Creare account Google Ads
2. Creare account Google Merchant
   1. compilare le info aziendali
   2. compilare la parte sul branding
3. In Merchant collegare account di Ads
   1. Scegliere "other google ads account"
   2. Cliccare  su link account

![](../assets/google-shopping.png)

## Annunci prodotti

Consigli pratici: [https://www.bigcommerce.com/blog/google-shopping-campaign-tips/\#chapter-3-the-importance-of-optimized-product-data](https://www.bigcommerce.com/blog/google-shopping-campaign-tips/#chapter-3-the-importance-of-optimized-product-data)

### Struttura Campagna

* Campagna
  * Product group
    * Subdivision

#### Suddivisione prodotti

serve per configurare il feed anche - labels o product type?

[https://searchengineland.com/starting-shopping-campaign-adwords-forget-know-keywords-191042](https://searchengineland.com/starting-shopping-campaign-adwords-forget-know-keywords-191042)

#### Shopping VS Ads PLA

[https://www.datafeedwatch.com/blog/google-shopping-vs-google-search-ads](https://www.datafeedwatch.com/blog/google-shopping-vs-google-search-ads)

[https://ppc.org/comparison-google-shopping-vs-google-adwords/](https://ppc.org/comparison-google-shopping-vs-google-adwords/)

## Feed

Le categorie prodotti si indicano con l'ID o con Categoria&gt;Sottocategoria

