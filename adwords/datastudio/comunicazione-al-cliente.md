# Comunicazione al Cliente

Gentile cliente,

di seguito le spiegheremo come compilare il file "consuntivo visite" per l'invio periodico dei dati sulle visite in cantina, indispensabili per l'analisi della campagna di visite in azienda promossa per la sua azienda.

**Aspetto del file "consuntivo visite"**

Si tratta di un foglio elettronico Excel, composto da 2 fogli:

* il primo, denominato "Registro Visite", in cui vengono riportati i dati delle visite in cantina
* il secondo, denominato "Riepilogo" riporta le tabelle riassuntive delle visite e viene compilato in parte automaticamente, e in parte da noi

**Compilazione del file "consuntivo visite"**

I dati richiesti, per entrambe le tipologie di prenotazione \(sito e fuori sito\), sono i seguenti:

* data di visita
* nr. di persone
* incasso visita
* incasso vino
* incasso totale
* prenotazione arrivata da:
* note

Nel campo "prenotazione arrivata da", colonna G, deve indicare il mezzo con il quale è arrivata la prenotazione, ad esempio:

* modulo, per tutte le prenotazioni arrivate tramite i moduli presenti sul suo sito
* telefono, per chi ha chiamato in azienda
* langhe.net, per le richieste arrivate attraverso la pagina personale della sua azienda pubblicata su langhe.net e di Cantine Aperte \(nel caso aderisca al servizio\)
* passaggio, per le visite arrivate senza prenotazione
* altro, specificare di cosa si tratta \(p.es altri portali\)

**Invio del file a Langhe.net  **

Per una corretta analisi, il file dovrà essere inviato nella settimana successiva il quadrimestre di riferimento, secondo quanto segue:

* I quadrimestre 01/01 al 30/04, invio la prima settimana di maggio
* II quadrimestre 01/05 al 31/08, invio la prima settimana di settembre
* III quadrimestre 01/09 al 31/12, invio la prima settimana di gennaio

La settimana di invio, riceverete una mail di richiesta di invio del file compilato come promemoria.

Grazie per la collaborazione,

lo Staff di Langhe.net

