# Analisi dei gruppi di annunci

L'analisi dei gruppi di annunci è un'attività complessa in cui vanno valutati i dati attuali e i dati storici.

L'analisi deve essere impostata a partire dall'ultima analisi svolta, e comparata con lo stesso periodo dell'anno precedente.

## Processo {#processo}

### Passo \#1 - Analisi miglioramenti

Come primo passo, devi verificare se i dubbi e le problematiche emerse con l'ultima analisi sono state risolte con gli interventi correttivi effettuati.

Se non ci sono stati cambiamenti, devi provare a **formulare un'ipotesi** sul perché i nostri interventi non abbiano sortito gli effetti desiderati.

### Passo \#2 - Analisi gruppi & domande

Successivamente, devi procedere all'analisi effettiva dei singoli gruppi. Le **domande** da porti quando analizzi un gruppo sono le seguenti.

1. Il gruppo ha portato **conversioni**?
2. Il **tasso di conversione** è abbastanza alto \(almeno 1%\)?  E' nella media della campagna?
3. Il **costo per conversione** è alto \(sopra i 35€\)? E' nella media della campagna?
4. Questi tre dati sono **migliorati** rispetto all'anno precedente?

**NB**: per aiutarti con l'analisi puoi utilizzare questo modello di excel \([link dropbox](https://www.dropbox.com/s/2tl84bfqhd5jnkw/Modell%20Analisi%20Gruppi%20Annunci.ods?dl=0)\), oppure lavorare direttamente su AdWords. I file di excel devono essere denominati nel modo seguente: aaaa.mm-Analisi Gruppi-cliente.ods

**NB2**: è possibile che tu raggolga molti dati e abbia molte molte considerazioni da fare. Puoi accoppiare al foglio di excel un file di word, dove puoi scrivere, per ogni gruppo, le problematiche individuate, e le soluzioni proposte.  
Usa lo stesso schema del file di xcel per **dare il nome** al file di word.

#### Caso \#1 - Risposta negativa

Se la risposta **è no a tutte le domande** \(o anche solo alle domande 2. 3. e 4.\), è possibile che il gruppo vada eliminato \(vedi capitolo "[eliminazione di un gruppo](#eliminazione)"\).

#### Caso \#2 - Risposta negativa alla domanda 4. {#caso-2}

Se la risposta all'ultima domanda è **"no, sono peggiorati"** devi procedere con l'analisi delle cause di questo peggioramento. Prova a **formulare un'ipotesi** facendoti alcune domande.

1. Di quanto è peggiorata la performance? E' possibile che il peggioramento sia così poco incisivo da rientrare in una naturale fluttuazione della performance?
2. Il **traffico **legato alle parole chiave dell'annuncio è aumentato?
3. E' possibile che l'aumento di traffico abbia portato dei **click poco qualificati**?
4. Il **CTR **è sceso?
   1. Se il CTR è **sceso**, è possibile che ci sia un altro advertiser e che i nostri annunci siano poco interesanti?
   2. Se il CTR è **aumentato**, è possibile che gli annunci diano un messaggio ambiguo? E' possibile che stiamo raccogliendo dei click poco qualificati?
5. Il problema è legato al tasso di conversione? E' possibile che ci siano dei problemi legati alla LP?
   1. Ci sono dei malfunzionamenti evidenti sulla LP?
   2. La LP è di facile navigazione e comprensione?
   3. Il messaggio contenuto negli annunci è fedelmente e chiaramente riportato anche sulla LP?

Una volta formulata l'ipotesi, prova a ideare degli **interventi correttivi** per riportare le performance al livello dell'anno precedente.

#### Caso \#3 - Risposte miste

Se hai risposto NO solo ad alcune domande, puoi comunque **approfondire **l'analisi utilizzanto i passi del "[Caso \#2](#caso-2)".

### Passo \#3 - Raccolta dati e considerazioni

Per ogni gruppo di annunci problematico, devi scrivere l'ipotesi da cui sei partito per ideare gli interventi correttivi, gli** **interventi correttivi individuati e le motivazioni che ti hanno spinto a fare questa scelta.

E' possibile che tu raggolga molti dati e abbia molte considerazioni da fare. Puoi accoppiare al foglio di excel un file di word, dove puoi scrivere, per ogni gruppo, le problematiche individuate, e le soluzioni proposte.

**NB: **Per dare il nome al file di word usa lo stesso schema del file di Excel.

Una volta pronta la documentazione, **rivedila insieme alla squadra** per vedere se un input esterno puoi aiutarti a vedere dei dettagli che ti erano sfuggiti.

### Passo \#4 - Implementazione

Schedula le attività da fare per correggere l'andamento della campagna su Active Collab.

Ricordati che ogni modifica della campagna deve essere segnata nel "Diario della Campagna".

Modifiche particolarmente importanti, come l'eliminazione di un gruppo di annunci, il cambio di budget, o una nuova LP devono anche essere segnati come commento su [Google Analytics](https://analytics.google.com/analytics/web/).

## Eliminazione di un gruppo

Se tutte le risposte alle [prime quattro domande](#processo) sono negative, è possibile che il gruppo debba essere eliminato.

Segui questi **due semplici passi** per decidere se un gruppo deve essere effettivamente eliminato.

#### Passo \#1 - Analisi temporale

Per prima cosa devi analizzare le performance del gruppo nel tempo: sono sempre state insoddisfacenti e/o di molto al di sotto della media? Riesci a trovare un motivo molto evidente per questo fatto?

Se le perfomance sono sempre state insoddisfacenti, e non c'è nessun motivo evidente per questo fatto, è possibile che il gruppo \(le sue parole chiave\) non porti un traffico abbastanza qualificato per l'offerta proposta.

Se invece le performance sono diventate insoddisfacendi nell'ultimo periodo \(o sono calate lentamente nel corso del tempo\), puoi vedere quanto scritto nel "[Caso \#2](#caso-2)".

#### \#Passo \#2 - Impatto

A questo punto devi solo più** **valutare l'impatto che avrà la disattivazione del gruppo sull'intera campagna:

1. Quante conversioni perderà la campagna?
2. Quanti soldi verranno risparmiati?
3. Gli altri gruppi più performanti hanno la capacità di riassorbire il traffico di questo gruppo?

Se la percentuale è effettivamente bassa, allora puoi disattivare il gruppo.

