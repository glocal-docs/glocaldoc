# Scrittura degli Annunci

Linee guida da utilizzare in caso di scrittura di nuovi annunci, oppure di revisione degli annunci creati negli anni passati.

### Visualizzazione degli annunci

Gli annunci sono composti da:

* 2 titoli di 30 caratteri ciascuno, separati da un - Quindi non bisogna scrivere una frase "spezzata" sui due campi
* 1 descrizione di 80 caratteri
* 2 campi URL per personalizzare l'indirizzo, di 15 caratteri ciascuno: usare le lettere maiuscole

Su mobile i titoli possono venire tagliati perchè i caratteri utilizzati non sono mono-space \(quindi 3 ooo occupano più spazio di 3 iii\). Avanza 3/4 caratteri in meno sul totale dei 2 titoli \(60 caratteri\), per evitare che vengano mostrati i ... di sospensione.

## Scrittura degli annunci

* Utilizza il file su DB &gt; Langhe - Campagne Adwords &gt; Modelli vuoti &gt; modello campagna
* Inizia pensando al concetto che vuoi scrivere e poi butta giù il testo. Ad esempio per Tom Roger:
  * esclusività del prodotto --&gt; scrivi un annuncio dove si parla dell'Edizione Limitata a 300 esemplari
  * personalizzazione --&gt; scrivi un annuncio dove si parla dell'incisione del tuo nome sul ciondolo
* Crea annunci che siano abbastanza diversi tra di loro.
* Non pensare troppo, butta giù cosa ti viene in mente anche a random. In un secondo momento potrai eliminare ciò che non ti convince.

### Ortografia e Sintassi

* Non è il caso di includere il nome dell'azienda, perchè viene visualizzato in automatico dall'annuncio
* Evita ripetizioni nei titoli
* Avanza 3/4 caratteri sulla somma totale dei titoli, se possibile
* Utilizza le maiuscole nei titoli e nel percorcoso personalizzato per mettere in evidenza determinate parole
* Non seguire le regole per la scrittura degli slug nel campo "percorso personalizzato": pensa alla facilità di lettura per l'utente finale
* Non utilizzare caratteri speciali come ? e ! nei titoli
* Non è necessario chiudere le frasi con un . punto

Documentazione Google Adwords:[https://support.google.com/adwords/answer/1704389?hl=it](https://support.google.com/adwords/answer/1704389?hl=it)

