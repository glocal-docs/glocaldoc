# Mail

## Creazione di una nuova casella
....

....

## Attivazione SMTP
....

....

## Parametri di configurazione
**Posta in entrata IMAP**

- Username / Email: indirizzo mail
- Password: la password
- Porta: 993
- Nome Server: pop.securemail.pro
- Sicurezza: SSL

**Posta in Uscita**
- Username / Email: smtp@nomedominio.tld
- Password: password
- Porta: 465
- Nome Server: authsmtp.securemail.pro

## Webmail

La webmail è accessibile di default, tramite certificato SSL, all'indirizzo [webmail.register.it](https://webmail.register.it).

### Attivazione indirizzo personalizzato

Per attivare la webmail su un dominio all'indirizzo `webmail.dominio.ext` devi:

1. Dalla home del pannello di controllo, scendere nella sezione "Accessi rapidi"
2. Cliccare su "personalizza la tua webmail"
3. Nella tab "Attiva la webmail" aggiungere la spunta per il dominio su cui vuoi attivare il servizio
4. Clicca il bottone "salva" a fondo pagina

!> **NB** La webmail su `webmail.dominio.ext` non è servita su https.

# Comandi utili

## Capire se un dominio è hostato sui nostri server o su register.

* Apri il prompt dei comandi
  * clicca su start
  * scrivi `cmd`
  * april l'applicazione suggerita "Prompt dei comandi"
* nel prompt scrivi `ping dominio.ext`
* batti invio
* se nella riga successiva compare l'indirizzo IP `81.201.5.180` vuol dire che è hostato sui nostri server