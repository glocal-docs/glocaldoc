# MySQL / phpMyAdmin

## Increment column value

```sql
update my_table  
set my_column  = my_column +1
```
## Find & Replace

```sql
update [TABLE] set [COLUMN] = replace([COLUMN], 'ORIGINAL TEXT', 'NEW TEXT');
```

Example:

```sql
update wp_4_termmeta set meta_key = replace(meta_key, 'long_description_2', 'text_2_text');
```

## Remove (multiple) meta values

```sql
DELETE FROM [TABLE] WHERE `meta_key` IN ([META KEY], [META KEY])
```

Example:

```sql
DELETE FROM wp_406_postmeta WHERE `meta_key` IN ('layout', '_avia_builder_shortcode_tree')
```