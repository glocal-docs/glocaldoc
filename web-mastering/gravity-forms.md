# Gravity Forms

### Preparazione

Loggati sul sito interessato come superamministratore. Se Gravity Forms non è già stato attivato, attivalo da "Plugin".

Una volta che Gravity Forms è stato attivato, scollegati dall'account di superamministratore e accedi nuovamente come amministratore.

---

### Creazione del modulo

Vai alla sezione "Moduli", clicca su "aggiungi nuovo" e dai un nome \(e una descrizione, se necessaria\) al nuovo modulo.

Adesso devi creare i campi che l'utente compilerà.

Nella sezione di destra ci sono i vari elementi che puoi trascinare nel modulo. Trascina gli elementi desiderati  
 ricordando che:

1. per inserire un campo che consenta di selezionare una sola voce devi usare "pulsante radio"
2. per inserire un campo che consenta le risposte multiple devi usare "caselle di verifica"
3. per tutti gli altri campi usa il nome campo adeguato \(es. per il nome usa il campo "nome"\)

**Come compattare il modulo**

Se hai bisogno di rendere più compatto il modulo e impostare due elimenti sulla stessa riga, vedi la documentazione qui [https://www.gravityhelp.com/documentation/article/css-ready-classes/](https://www.gravityhelp.com/documentation/article/css-ready-classes/), ma in linea di massima, nel campo "CSS Class Name" aggiungi:

* gf\_left\_half \(per avere l'elemento più in alto a sx\) 
* gf\_right\_half \(per avere l'elemento sotto a dx\)

**Condizioni logiche**

Se hai bisogno di far spuntare un campo in base alla risposta che viene data sul modulo \(es. "vuoi prenotare anche la cena?" risposta SI / NO\), vai alla sezione "avanzato" e spunta "attivare condizione logica".

Devi poi completare la formula in base ai dati di cui hai bisogno.

**Per il bottone**

Clicca su "impostazioni modulo" e alla sezione "pulsante modulo" personalizza il testo del bottone.

**Per le conferme**

Clicca su "conferme" nella barra laterale e scegli "re-indirizza" inserendo il link della pagina che vuoi venga visualizzata una volta che il modulo è stato inviato dall'utente \(solitamente questa è la scelta consigliata per monitorare correttamente le prenotazioni da Google Analytics\).

Generalmente, per i moduli su cui si vuole impostare un Goal su Google Analytics reindirizziamo a una pagina di "Grazie" che creiamo per lo scopo.

Eventualmente, per moduli più semplici \(raccolta di informazioni o contatti\) puoi impostare un messaggio di testo \(es. Grazie per averci contattato, risponderemo al più presto alla tua richiesta\).

---

### Creazione delle notifiche

Le notifiche avvisano:

* noi e i nostri clienti che un modulo è stato compilato
* l'utente del sito che la richiesta è andata a buon fine e che riceverà una risposta a breve

**Notifica amministratore**

Clicca su notifiche e su "notifica amministratore" per modificare la notifica.

_Invia a un'email_  
Cambia il campo con l'indirizzo email di chi deve ricevere la prenotazione, quindi noi o i nostri clienti.

_Nome mittente_  
Inserisci nome e cognome dell'utente cliccando sulla freccetta "merge tags". Ricordati di lasciare uno spazio tra le due tag, se no vengono scritti appiccicati.

_Email mittente_  
Inserisci la mail verificata del sito, nostro o del cliente.

_Rispondi a_  
Inserisci la mail dell'utente cliccando sulla freccetta "merge tags".

_CCN_  
Inserisci reservation@langhe.net.

_Oggetto_  
Inserisci l'oggetto della mail e, se vuoi, aggiungi alcuni campi compilati dall'utente cliccando sulla freccetta in fondo a destra "merge tags" \(es. nel caso volessi far stampare nell'oggetto la data di arrivo o di visita\). Ricordati che qui devi anche inserire il dominio del sito, magari preceduto da una \|.  \(es: Nuova richiesta di visita in cantina \| langhe.net\).

_Messaggio_  
Nel corpo del testo della email inserisci lo shortcode {all\_fields} così da poter vedere tutti i dati compilati dall'utente. Se vuoi, puoi scrivere un testo prima di questo shortcode es. - Ciao, è arrivata una nuova prenotazione, qui sotto tutti i dati {all\_fields}.

**Notifica utente**

Clicca su notifiche e su "notifica utente" per modificare la notifica.

_Invia a_  
Spunta "seleziona un campo" e poi seleziona "email"

_Nome mittente_  
Inserisci il nome del cliente che generalmente si interfaccia con la clientela e aggiungi il dominio dopo una \| \(es. Enrico \| langhe.net\).

_Email mittente_  
Inserisci la mail verificata del sito nostro o del cliente.

_Rispondi a_  
Lascia il campo vuoto.

_CCN_  
Lascia il campo vuoto.

_Oggetto_  
Inserisci l'oggetto della mail \(es: Richiesta inviata con successo\).

_Messaggio_  
Nel corpo del testo della email inserisci lo shortcode {all\_fields} così che l'utente possa rivedere tutti i dati che ha compilato nel modulo. Scrivi un testo prima di questo shortcode e ricordati di firmare la mail con tutti i dati dell'azienda.

---

## Testi delle notifiche

Sono esempi e puoi copiarli, ma ricorda di incollarli come testo semplice.

**N.B. **

I campi segnati in \[PARENTESI QUADRE\] sono da ricompilare a manovella \(eliminando le parentesi\).

I campi segnati in {PARENTESI GRAFFE} sono da ricompilare con l'inserzione dei campi di gravity forms, tranne che per i campi {date\_dmy} e {all\_fields} che possono rimanere sempre uguali \(rispettivamente la data di invio del modulo e l'elenco di tutti i campi compilati\).

Oggetti notifiche per moduli non menzionati di seguito:

* Compilazione modulo “{form\_title}” - \[dominio\]
* Richiesta prenotazione dal modulo "{form\_title}" \| \[dominio\]
* Richiesta informazioni dal modulo "{form\_title}" \| \[dominio\]

### **Prenotazione camera**

Oggetto notifica amministratore:  
Nuova richiesta soggiorno per il {DATA CHECKIN DA SOSTITUIRE} - \[DOMINIO\]

Testo notifica amministratore:

Ciao \[NOME REF SITO\],

hai ricevuto una richiesta di soggiorno per il {DATA CHECK-IN DA SOSTITUIRE}.

Ecco i dettagli della richiesta:

{all\_fields}

\*\*\*

Oggetto notifica utente:  
Riepilogo della tua richiesta di soggiorno a \[AZIENDA\] del {date\_dmy}

Testo notifica Utente:

Ciao {NOME DA SOSTITUIRE},

ti ringraziamo per aver richiesto un soggiorno nella nostra \[STRUTTURA\] per il periodo {DATA CHECKIN DA SOSTITUIRE} - {DATA CHECKOUT DA SOSTITUIRE}. Ti ricontatteremo al più presto per una conferma!

Qui sotto trovi un riepilogo della tua richiesta.

{all\_fields}

Se hai qualche domanda o vuoi fare qualche correzione, rispondi pure a questa email, oppure contattaci qui:

\[FIRMA CON CONTATTI\]

### **Modulo di contatti ITA**

Oggetto notifica amministratore:  
{OGGETTO DA SOSTITUIRE} - via modulo di contatti \[DOMINIO\]

Testo notifica amministratore

Ciao \[NOME REF SITO\],

è arrivato un nuovo messaggio dal modulo di contatti.

Ecco i dettagli della richiesta

{all\_fields}

\*\*\*

Oggetto notifica Utente:  
Riepilogo del tuo messaggio inviato a \[AZIENDA\]

Testo notifica Utente

Ciao {NOME DA SOSTITUIRE},

ti ringraziamo per averci contattato, ti risponderemo al più presto.

Qui sotto trovi un riepilogo del tuo messaggio.

{all\_fields}

Se hai qualche domanda o vuoi fare qualche correzione, rispondi pure a questa email, oppure contattaci qui:

\[FIRMA CON CONTATTI\]

### **Modulo contatti ENG**

Oggetto notifica Utente:  
Details of your request at \[AZIENDA\]

Testo notifica Utente

Hi {NOME DA SOSTITUIRE},

thank you for contacting us, we will answer you as soon as possible.

Here you can find the details of your request

{all\_fields}

If you need any further information or if you would like to make a correction, feel free to answer this email, or contact us at:

\[FIRMA CON CONTATTI\]

### **Visita in cantina ITA**

Oggetto notifica Amministratore:  
Nuova richiesta visita in cantina per il {DATA DA SOSTITUIRE} - \[DOMINIO\]

Testo notifica Amministratore

Ciao \[NOME REF SITO\],

è stata richiesta una visita in cantina per il {DATA DA SOSTITUIRE}.

Ecco i dettagli della richiesta

{all\_fields}

\*\*\*

Oggetto Notifica Utente  
Riepilogo della tua richiesta di visita in Cantina del {date\_dmy}

Testo notifica Utente

Ciao {NOME DA SOSTITUIRE},

ti ringraziamo per aver scelto di venire a trovarci in cantina. Ti ricontatteremo al più presto per una conferma!

Qui sotto trovi un riepilogo della tua richiesta.

{all\_fields}

Se hai delle domande o vuoi fare qualche correzione, rispondi pure a questa email, oppure contattaci qui:

\[FIRMA CON CONTATTI\]

### **Visita in cantina DE**

Testo notifica utente:

Guten Tag!

Wir bedanken uns für Ihre Anfrage und werden Ihnen so schnell wie möglich antworten!

Nachstehend finden Sie eine detaillierte Auflistung Ihrer Anfrage:

{all\_fields}

Wenn Sie mit uns in Kontakt treten möchten, können Sie dafür eine der folgenden Möglichkeiten wählen:

### **Prenotazione tavolo ITA**

Oggetto notifica amministratore:  
Nuova richiesta di prenotazione tavolo per il {DATA DA SOSTITUIRE} - \[DOMINIO\]

Testo notifica amministratore:

Ciao \[NOME REF SITO\],

hai ricevuto una richiesta di prenotazione per il {DATA DA SOSTITUIRE}, alle ore {ORA DA SOSTITUIRE}.

Ecco i dettagli della richiesta:

{all\_fields}

\*\*\*

Oggetto notifica Utente:  
La tua richiesta di prenotazione per il {DATA DA SOSTITUIRE} è stata inviata correttamente - {azienda}

Testo notifica Utente:

Ciao {NOME DA SOSTITUIRE},

grazie per la tua richiesta. Ti risponderemo al più presto.

Qui sotto trovi un riepilogo della tua prenotazione:

{all\_fields}

Se avessi bisogno di ricontattarci puoi rispondere a questa email.

\[FIRMA CON CONTATTI\]

### **Prenotazione tavolo ENG**

Oggetto notifica Utente:  
Your reservation request for the {DATA DA SOSTITUIRE} has been sent successfully - {azienda}

Testo notifica Utente:

Hi {NOME DA SOSTITUIRE},

thank you for your request. We will answer you as soon as possible.

Here you can find the details of your reservation:

{all\_fields}

If you need to contact us again, feel free to answer this email.

\[FIRMA CON CONTATTI\]

Fonte di questi testi sopra: [https://www.evernote.com/shard/s31/nl/3370277/2e9ca056-990e-491b-8a56-c770340b699e](https://www.evernote.com/shard/s31/nl/3370277/2e9ca056-990e-491b-8a56-c770340b699e)

Qui ci sono anche due moduli prenotazione camere \(ita e eng\) del vignetodiroddi.com che puoi importare \(nella sezione Moduli&gt;Importa/Esporta&gt;Importazione Moduli\) e modificare al bisogno: [https://www.dropbox.com/s/rirlod6mmwqxl2w/gravityforms-export-2014-01-02.xml](https://www.dropbox.com/s/rirlod6mmwqxl2w/gravityforms-export-2014-01-02.xml)

