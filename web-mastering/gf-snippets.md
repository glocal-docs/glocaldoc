```js
<script type="text/javascript">
gform.addFilter( 'gform_datepicker_options_pre_init', function( optionsObj, formId, fieldId ) {
    if ( (formId == 1 && fieldId == 4) || (formId == 4 && fieldId == 4) || (formId == 6 && fieldId == 4) ) {
        optionsObj.firstDay = 1;
        optionsObj.beforeShowDay = function(date) {
            var day = date.getDay();
            return [(day == 0 || day == 6)];
        };
    }
    return optionsObj;
});
</script>

```