# Updates

## WooCommerce Multilingual

### Break / Abort rules
in WCML `compaitibility/class-wcml-table-rate-shipping.php` remove:

```
add_filter( 'woocommerce_shipping_table_rate_is_available', [ $this, 'shipping_table_rate_is_available' ], 10, 3 );
```

## Fail2Ban

sostituire `manage_options` con `create_sites`

Potrebbe essere sostituito con `install_plugins` che va bene per tutti gli enviroment e non solo in multisite.