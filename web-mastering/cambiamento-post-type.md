# Cambiamento post type

Se durante una migrazione di un sito hai bisogno di trasformare un CPT \(ad es. portfolio\) in un altro CPT \(ad es Vino\), segui queste istruzioni.

## Query

La query da usare è la seguente

`update TABLE_NAME set FIELD_NAME = replace(FIELD_NAME, 'TEXT TO FIND', 'TEXT TO REPLACE');`

dove

* `TABLE_NAME` è il nome della tabella su cui intervenire
* `FIELD_NAME` è il nome della colonna
* e `TEXT TO FIND` e `TEXT TO REPLACE` sono i testi da cercare/sostituire

NB: è sempre meglio eseguire un dry run delle query sul database per avere un'idea di cosa succede.

NB2: i nomi indicati qui, per esempio `wine_category` o `portfolio` sono indicativi, ricrdati sempre di cercare e usare i nomi veri dei post/tassonomie che vuoi sostituire.

## Tabelle

Le tabelle in cui effettuare le sostituzioni sono le seguenti.

### Post

La tabella in cui sostituire il tipo di post è `wp_posts` \(se sei su wp\_315\_term\_taxonomyricordati di usare l'ID es: `wp_315_posts`\) dove bisogna cercare nella colonna `post_type` il tipo di post vecchio e sostituirlo con quello nuovo.

### Taxonomies

Se al post sono collegate delle tassonomie, e il nome di queste tassonomie cambia, devi cercare il nome della vecchia tassonomia e sostituirlo con quello nuovo nella tabella `wp_term_taxonomy` \(ricordati di usare l'ID se sei su **multisite **es: `wp_315_term_taxonomy`\).

La colonna su cui intervenire è `taxonomy`.

### WPML

Se WPML è attivo bisogna andare anche a cambiare le referenze per la traduzioni, in modo che gli elementi rimangano correttamente collegati.

La tabella su cui intervenire è `wp_icl_translations` \(se sei su multisite ricordati di usare l'ID del sito es: `wp_315_icl_translations`\).

La colonna su cui intervenire è `element_type`. E le ricerche sono da fare sia per il tipo di post che per i termini della tassonomia.

Il nome del tipo di post deve essere prepeso con `post_` e il nome della tassonomia tassonomia con `tax_`, per esempio:

Per cercare e sostituire il tipo di post portfolio con il tipo di post vino devi usare:

* termine da cercare `post_portfolio`
* termine da sostituire `post_wines`

Per cercare e sostituire la tassonomia "Categoria portfolio" con la tassonomia "Categoria vino" devi usare:

* termine da cercare `tax_portfolio_category`
* termine da sostituire `tax_wine_category`



