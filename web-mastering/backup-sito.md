# Procedure backup e duplicazione

## Backup sito da rendere al cliente:

### Istruzioni da inviare al cliente

1. installare wordpress
2. caricare la cartella wp-content (sovrascrivendo se necessario)
3. importare il database

E' possibile che sia necessario fare un find & replace delle path delle immagini nel db (dipende da come è configurato il server)

## Duplicazione sito su Multisite

- creare un nuovo sito con gli utenti corretti
- attivare eventuali plugin etc
- Duplicare le seguenti tabelle, utilizzando l'opzione per rinominarle E quella "solo dati" 
- - wp_406_commentmeta
- - wp_406_comments
- - wp_406_postmeta
- - wp_406_posts
- - wp_406_termmeta
- - wp_406_terms
- - wp_406_term_relationships
- - wp_406_term_taxonomy