# WP CLI

\[descrizione\]

## Installazione

### Windows

https://deliciousbrains.com/complete-guide-to-installing-wp-cli/#install-wp-cli-on-windows

- creare una cartella `wp-cli` in C:\
- salvare il file https://raw.github.com/wp-cli/builds/gh-pages/phar/wp-cli.phar nella cartella appena creata
- confermare che il file funzioni con il comando `php C:\wp-cli\wp-cli.phar --info`
- aggiungere il file in path `setx PATH "%PATH%;C:\wp-cli" /m` (il terminal deve essere runnato as administrator)
- creare un file wp.bat nella cartella e scriverci
```bat
@ECHO OFF
php "c:\wp-cli\wp-cli.phar" %*
```
- verificare che funzioni `wp --info`

#### Aggiungere in path su git bash
https://github.com/wp-cli/wp-cli/issues/5413

Create another file wp without any extension into the C:\wp-cli\ directory and paste below code:

```shell
#!/usr/bin/env sh

dir=$(d=${0%[/\\]*}; cd "$d"; pwd)

# See if we are running in Cygwin by checking for cygpath program
if command -v 'cygpath' >/dev/null 2>&1; then
   # Cygwin paths start with /cygdrive/ which will break windows PHP,
   # so we need to translate the dir path to windows format. However
   # we could be using cygwin PHP which does not require this, so we
   # test if the path to PHP starts with /cygdrive/ rather than /usr/bin
   if [[ $(which php) == /cygdrive/* ]]; then
       dir=$(cygpath -m $dir);
   fi
fi

dir=$(echo $dir | sed 's/ /\ /g')
"${dir}/wp-cli.phar" "$@"
```


## Flush rewrite rules

#### Single site

`wp --url=[WEBSITE URL] rewrite flush`

#### Multisite

`for SITE in $(wp site list --field=url); do wp --url=$SITE rewrite flush; done;`

#### Multisite \(verbose\)

`for SITE in $(wp site list --field=url); do echo $SITE && wp --url=$SITE rewrite flush; done;`

## Update all posts (to test)

#### Single site

!> Parametri da cambiare POST_TIPE. Inoltre, ricordati di settare la data con la data corrente (AAAA-MM-GG).

```wp post list --post_type=POST_TYPE fields=ID,post_title,post_modified```

```for POST in $(wp post list --post_type=POST_TYPE --field=ID); do echo $POST && wp post update $POST --post_modified="2021-01-14 23:55:55"; done```

#### Multisite

!> Parametri da cambiare POST_TIPE e SITE_URL. Inoltre, ricordati di settare la data con la data corrente (AAAA-MM-GG).

`wp post list --url=SITE_URL --post_type=POST_TYPE fields=ID,post_title,post_modified`

`for POST in $(wp post list --url=SITE_URL --post_type=POST_TYPE --field=ID); do echo $POST && wp post update $POST --url=SITE_URL --post_modified="2021-01-14 23:55:55"; done`

## Delete all posts
`wp post delete $(wp post list --post_type='POST_TYPE' --format=ids --url=SITE_URL) --url=SITE_URL`

## Delete all terms from tax
`wp term list TAX_SLUG --field=term_id --url=SITE_URL | xargs wp term delete TAX_SLUG --url=SITE_URL`

## Import files in media library
`wp media import ~/Pictures/**\/*.jpg --url=SITE_URL`

## Update urls
`wp search-replace http://www.olddomain.com http://www.newdomain.com`

## Resize all thumbs
Documentazione completa: https://developer.wordpress.org/cli/commands/media/regenerate/

`wp media regenerate --yes`

Oppure se il FS è protetto

`sudo -u www-data wp media regenerate --yes`

!> Se sei su multisite, ricorda di inserire il parametro --url=dominio.tld.

## Update WC database
`for SITE in $(wp site list --field=url); do echo $SITE && wp --url=$SITE wc update; done;`