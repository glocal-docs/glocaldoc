# SSH

## Move local file to remote

`scp -r -P 2223 local_folder  user@server:/path/to/remote/folder/local_folder`

## Activate SSL cert with certbot

Login on the balancer, and in the root folder `/home/langhe` execute:

`./creacertificato.sh domain.tld`

