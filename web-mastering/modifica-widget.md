# Modifica Widget

**NB**: Nel caso in cui dovessi modificare delle parti di widget che contengono del codice Javascript, è necessario loggarsi con l'account superamministratore. Se non utilizzi questo account le tue modifiche non saranno salvate.

1.  Dalla dashboard del sito, vai su ‘aspetto’ &gt; ‘widget’
   * Nella **parte dx dello schermo**, trovi le diverse aree "widgetizzabili" del sito \(es: sidebar, sidebar blog, footer etc.\).
   * Nella **parte sx dello schermo**, trovi tutti i widget con le loro descrizioni. Una volta scelto ciò che fa al caso tuo, ti basterà trascinarlo all’interno dell'area widgetizzabile in cui vuoi che venga visualizzato l’elemento.
2. Per aiutarti nel processo di modifica, visualizza sempre il frontend del sito, così che le tue modifiche siano più intuitive.

Una volta eseguite modifiche, controlla sempre da frontend il lavoro che hai fatto \(compresi i link, se ne hai utilizzati\).

## Tipi di widget

La maggior parte dei widget ha un utilizzo intuitivo e ti basta leggere le loro descrizioni, ma per alcuni è meglio che tu abbia qualche referenza in più:

* testo: puoi utilizzarlo se ciò che scrivi non deve essere accompagnato da html
* html personalizzato: è l’opzione migliore in caso in cui avessi dell’html da utilizzare



