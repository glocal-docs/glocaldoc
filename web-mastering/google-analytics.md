# Google Analytics

## Remove tresholds in reports
To address the 'Thresholding applied' issue in the GA4 reporting interface, you can now disable the toggle button associated with 'Include Google signals in reporting identity' without making changes in 'Reporting Identity' options. To do this, follow these steps:

1) Navigate to the settings in your GA4 account.
2) Select 'Data Collections' under 'Data Settings.'
3) Locate the toggle button next to 'Include Google signals in reporting identity.'
4) Turn off this toggle button.

Please note that when you disable 'Include Google signals in reporting identity,' Google Analytics will no longer display insights and detailed information in your reports based on data from signed-in and consented Google account users who visit your website and apps. However, this action will effectively remove the 'Thresholding applied' warning notification from the reporting interface.