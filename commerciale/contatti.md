# Contatti

## Hai trovato un contatto interessante?

Se hai trovato un contatto interessante, ma non sei un commerciale, procedi nel modo seguente.

#### \#1 - Carica il contatto

Carica il contatto su [Odoo](https://crm.glocalweb.it) seguendo le linee guida. Ricordati di controllare se il contatto esiste già!

#### \#2 - Se ha manifestato un interesse

Se il contatto ha manifestato un interesse concreto su un prodotto o un servizio, crea un deal legato al contatto.

Scrivi nel deal a che prodotto / servizio è interessato il cliente.

Metti al deal una data di scadenza a 1 o 2 giorni.

#### \#3 - Se non ha manifestato un interesse

Se non ha manifestato un interesse concreto, aggiungi una nota al contatto e poi assegna una task al commerciale \(con scadenza vicina\) per farglielo contattare.



