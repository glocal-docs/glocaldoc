# Prezzi indicativi
Hai bisogno di comunicare velocemente dei prezzi indicativi ad un cliente? Usa le tabelle qui sotto.

Ultimo aggiornamento; 16/05/2022. Tariffe a 50€/ ora.

## Siti web

### Grafica di base
Grafica di base con messa online di un piccolo sito da qualche pagina: 1.500€

### Implementazione di grafica già fatta
Implementazione di interfaccia già prodotta (se complessa, se no far riferimento a `grafica di base`), caricamento contenuti: **~ 3.000€**

### Sito con studio grafico dedicato
Realizzazione di studio grafico dedicato, implementazione e caricamento contenuti: **~ 4.000€**

### Sito con studio di branding preventivo
Realizzazione di studio di branding (revisione logo, tipografica istutizionale, elementi grafici aziendali e palette colori aziendale), design web dedicato, implementazione e caricamento contenuti: **~ 5.000€**

## Content

### Pagina istuzionale / Landing page
400€. Se le pagine aumentano, scende il prezzo per pagina. Possiamo contare un 225€ in media quando sono almeno 4-5 pagine.

Ottimizzate per SEO / CRO.

### Traduzione (a pagina)
Madrelingua 50€

### Scrittura articolo blog
150€.
