## Chi siamo

An awesome project.

## Questa guida

### Troubleshooting

Se non riesci a far partire docsify sul tuo computer locale, fai precedere il comando con `npx`. Es:

`npx docsify serve`

## Come Lavoriamo

\[introduzione\]

### Riunioni & Meeting

Se vuoi organizzare una riunione, puoi farlo invitando i partecipanti tramite Google Calendar.

Ogni volta che organizzi una riunione, dovresti creare un file con l'ordine del giorno \(ODG\), così che gli altri partecipanti possano vedere di cosa tratta la riunione, prepararsi in ancitipo, e lasciarti eventuali osservazioni.

Per fare il file dell'ordine del giorno devi:

1. Entrare su Google Drive
2. Navigare fino alla cartella `Glocal - Clienti / Riunioni`
3. Duplicare il modello `_aaaa.mm.dd - Nome Riunione` compilando il nome in modo corretto:
   1. Elimina l'underscore
   2. Compila il nome con `anno.mese.giorno - nome della riunione`, es: `2018.12.05 - Processi commerciali`
4. Sul nuovo file, compila l'ordine del giorno nel modo più dettagliato possibile
5. Aggiungi il link al file nella descrizione dell'evento su Google Calendar

Questo file deve essere usato anche per compilare il verbale della riunione.

#### Verbale

Ogni riunione deve terminare con un verbale chiaro e completo di quanto discusso e deciso durante l'incontro.

Ogni action item deve essere assegnato ad un responsabile, che dovrà distribuirlo nei relativi canali \(clickup, etc\).

> An awesome project.

!> An awesome project.

?> An awesome project.
