# Creazione della Campagna

1. Loggati su Adwords

2. Nel menu a sinistra scegli Account

3. Nelle tab in alto scegli Gestione e clicca sul + nel pallino blu, crea nuovo account

4. Compila i dati richiesti come segue:

   1. Tipo: account Adwords

   2. Nome: Nome Cliente

   3. Utenti: lasciare vuoto

5. Clicca su Crea Account e prosegui.

6. Entra nell'Account appena creato e, dal menu di sinistra, scegli Campagne

7. Clicca sul + nel pallino blu, crea nuova campagna, Tipologia: Ricerca.

8. A seconda della tipologia di campagna si sceglie il parametro successivo: ad esempio Tom Roger è impostato come Vendia, le cantine come Lead. Clicca su Continua.

9. Nella pagina successiva dai un nome alla campagna e poi compili come segue, sempre a seconda del caso:

   1. Reti: V Rete di ricerca - NO Rete display

   2. Località: Italia

   3. Lingue: Italiano

   4. Offerta: massimizza i CLIC

   5. Budget: inserire il budget giornaliero con la virgola

   6. Metodo pubblicazione: standard

   7. Data inizio: oggi se si è pronti a partire subito \(si può sempre mettere in pausa\) oppure una data futura per quando si pensa di far partire la campagna. In questo caso crea una task data su active per ricordarti quando parte la campagna.

   8. Impostazione aggiuntive:

      1. Rotazione annunci: non ottimizzare --&gt; questa impostazione andrà cambiata dopo alcuni mesi

10. Clicca su Salva e Continua.

11. Su active crea una task a un mese dalla partenza della campagna per ricordarti di andare a cambiare la rotazione degli annunci. Se dopo un mese i dati raccolti non sono ancora sufficienti, posticipa la task di un altro mese.

### **Creazione dei Gruppi di Annunci**

1. Dal menu di sinistra seleziona Gruppi di Annunci e clicca su Crea gruppo di annunci.

2. Imposta il nome con Parola Chiave - Tipo di corrispondenza. Ad esempio: Gioiello - BMM. Le sigle per i tipi di corrispondenza sono:

   1. BMM --&gt; Broad Match Modifier

   2. EM --&gt; Exact Match

   3. PM --&gt; Phrase Match

3. Inserisci le parole chiave per il gruppo.

4. Clicca su Salva e continua.

5. Inserisci il tuo primo annuncio compilando i campi a partire dal file su DropBox della Campagna. L'UR finale corrisponde all'indirizzo della Landing Page.

6. Puoi creare un solo annuncio e terminare la procedura guidata, per poi proseguire con la creazione degli altri annunci direttamente nel gruppo, oppure inserire tutti gli annunci con questa procedura. E' indifferente. NB: ricordati che gli annunci con lettere maiuscole nei titoli hanno CTR più alti.

7. Una volta creati tutti gli annunci inserisci le parole a corrispondenza inversa \(se ce ne sono\), per il singolo gruppo o per la campagna intera.**Consiglio**: se ciascun gruppo ha delle parole chiave a corrispondenza inversa specifiche, caricale non appena termini di pubblicare gli annunci di quel gruppo.

8. Scegli Parole chiave dal menu di sinistra.

9. Dalla tab in alto scegli Parole chiave a corrispondenza inversa.

10. Clicca sul più nel bollino blu e inserisci le parole chiave singolarmente o come lista.



