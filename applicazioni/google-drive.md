# Google Drive

## Installazione

Di seguito le istruzioni per collegarsi alla nostra piattaforma di filesharing.

### Passo \#1 - Scarica le applicazioni

Scarica l'applicazione desktop da qui: [https://www.google.com/drive/download/](https://www.google.com/drive/download/)

e procedi con l'installazione.

### Passo \#3 - Configurazione

Una volta terminata l'installazione, puoi configurare Google Drive come preferisci.

Per accedere al pannello delle opzioni, fai click destro sull'icona di drive nell'area di notifica, e poi clicca sull'icona dell'ingranaggio e scegli "Preferenze":

![](../assets/gdrive_1.png)

#### Tipo di accesso

La prima cosa che devi scegliere è il tipo di accesso ai file:

- L'opzione "Accedi in streaming ai file" occupa meno spazio sul tuo computer, ma se non hai la connessione, solo i file che hai scelto per l'accesso offline saranno disponibili.
- L'opzione "Esegui il mirroring dei file" funziona come Nextcloud o Dropbox: tutti i file saranno sempre disponibili sul tuo computer, anche senza connessione internet, ma l'installazione occuperà più spazio.

Se non hai esigenze particolari, consigliamo la seconda opzione.

![](../assets/gdrive_2.png)

#### Location della cartella

Di default la cartella di Google Drive viene salvata nella cartella del tuo utente. Se vuoi spostarla, riapri le preferenze come indicato sopra. Nella pagina delle preferenze clicca sull'icona dell'ingranaggio in alto a destra.

![](../assets/gdrive_3.png)

E da qui scegli la cartella dove vuoi sincronizzare il tuo GDrive.

![](../assets/gdrive_4.png)

## Utilizzo
Per i file condivisi con tutto il team, vanno usati di "Drive Condivisi", strutturati attualmente come le cartelle di NextCloud.

