# NextCloud

Di seguito le istruzioni per collegarsi alla nostra piattaforma di filesharing.

### Passo \#1 - Accesso alla versione web

Per installare NextCloud sul tuo dispositivo, devi prima eseguire l'accesso alla versione web.

L'url è [https://nextcloud.glocalweb.it](https://nextcloud.glocalweb.it).

Il nome utente e la password sono quelli che ti sono stati inviati via mail.

### Passo \#2 - Scarica le applicazioni

Una volta eseguito l'accesso, devi scaricare le applicazioni per il tuo computer e/o il tuo cellulare:

* Windows - [scarica](https://download.nextcloud.com/desktop/releases/Windows/Nextcloud-2.6.0-setup.exe)
* MacOS - [scarica](https://download.nextcloud.com/desktop/releases/Mac/Installer/Nextcloud-2.6.0.20190927.pkg)
* Android - [scarica da Google Play](https://play.google.com/store/apps/details?id=com.nextcloud.client)
* iOS \(iPhone e iPad\) - [scarica da Appstore](https://itunes.apple.com/us/app/nextcloud/id1125420102?mt=8)

Una volta scaricata, procedi all'installazione

### Passo \#3 - Primo avvio

Una volta scaricate e installate, aprendo l'applicazione next cloud ti verrà presentata questa schermata. Come opzione scegli "accedi".

![](../assets/nextcloud_1.png)

### Passo \#4 - Indirizzo server

Nella schermata successiva, alla voce "Indirizzo del server", inserisci `https://nextcloud.glocalweb.it` e poi clicca su "successivo".

![](../assets/nextcloud_2.png)

### Passo \#5 - Autorizza l'accesso

Si aprirà in automatico una finestra del browser, clicca su "Accedi" \(se NON hai eseguito l'accesso web nel primo passo, dovrai inserire il tuo user e password\).

![](../assets/nextcloud_3.png)

Nella schermata successiva devi cliccare su "Accorda Accesso".

![](../assets/nextcloud_4.png)

A questo punto puoi chiudere la finestra del browser.

### Passo \#6 - Scegli la cartella

Ritornato nell'applicazione, puoi scegliere dove mettere la cartella di nextcloud sul tuo computer.

Per concludere la procedura, clicca su connetti.

![](../assets/nextcloud_5.png)

### Passo \#7 - Fine ;-\)

Se devi installarlo su un'altro dispositivo, ripeti la procedura dal punto 1.

