# Blog

## Articoli tipo "Tutorial"

#### Video

Per video brevi si possono utilizzare delle GIF animate.

Le GIF devono essere registrate a 1280px \(720p\) e ritagliate per mostrare solo la parte di video interessate.

#### Immagini

Le immagini / screenshot possono essere annotate con Evernote + Skitch

Una volta inserite nell'editor, tutte le immagini devono avere una didascalia che descriva il loro contenuto.

L'immagine in evidenza \(e la prima immagine del post\) non devono avere annotazioni.

#### Nomi

I nomi dei bottoni o delle sezioni vanno "Virgolettati"



